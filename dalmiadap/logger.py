import logging
from logging.handlers import TimedRotatingFileHandler
import os


def get_logger(app_name):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    formatter = logging.Formatter(
        '%(asctime)s- %(threadName)s- %(name)s- %(levelname)s- %(message)s', "%b %d %H:%M:%S")
    file_path = os.path.join(BASE_DIR, 'logs/'+str(app_name)+'/')
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    log_name = get_log_name(app_name)
    handler = TimedRotatingFileHandler(file_path + str(log_name)+'.log',
                                       when='H',
                                       backupCount=72)
    handler.setFormatter(formatter)

    logger = logging.getLogger(log_name)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


def get_log_name(app):
    log_name = ''
    app = app.split('_')
    for i in app:
        log_name = log_name+i[0].upper()+i[1::].lower()
        log_name = log_name+'_'
    log_name = log_name[0:len(log_name)-1]
    return log_name
