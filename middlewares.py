
from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin
from app.models import UserMaster, OutletDetails
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse


class ExceptionMiddleware(MiddlewareMixin):

    def process_exception(self, request, exception):
        print("Entered at process_exception in ExceptionMiddleware, Exception occured of type " + str(type(exception)) + ", Exception msg: " + str(exception) + ".")
        import traceback
        print(traceback.format_exc())
        data = {
            "info": "Something went wrong, Please try again later.",
            "status": False,
            "status_code": 500,
            "success": "FAILURE",
            "msg": "Exception occured, Exception msg: " + str(exception) + "."
        }
        return JsonResponse(data)

    def process_request(self, request):
        print("request.path" + str(request.path) + ", Logged-in user: " + str(request.user) + ".")
        if '/admin/app/outletdetails/' in request.path and 'change' in request.path:
            try:
                trying_to_access = request.path.split('/')[-2]
                so_code = UserMaster.objects.filter(
                    username=request.user.username).first().emp_id
                if request.user.is_superuser and request.user.designation == 'SO':
                    qs = OutletDetails.objects.filter(
                        so_code=so_code).values('dap_id')
                    if trying_to_access not in qs:
                        raise PermissionDenied
            except Exception as e:
                print(str(e))
