
def prepare_response(message, status, success, info, responseCode=200, data = ""):
    res = {}
    res['responseCode'] = responseCode
    res['data'] = data 
    res['message'] = message 
    res['status'] = status 
    res['success'] = success 
    res['info'] = info    
    return res