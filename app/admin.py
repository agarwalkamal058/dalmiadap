from django.contrib.auth.admin import GroupAdmin
from django.contrib import admin
from django.contrib.auth import get_permission_codename
from django.contrib.auth.admin import UserAdmin
from .models import OutletDetails, OutletMaster, BrandsList, BrandMaster, UserMaster, DesignationMaster, RegionMaster, StateMaster, CityMaster, DistrictMaster, BlockTalukaMaster, GeoLocationMaster
from django import forms
from django.contrib.admin.helpers import ActionForm
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
import operator
from django.utils.html import mark_safe, format_html
from datetime import datetime
from django.urls import reverse
from django.urls import resolve
from django.contrib.auth.models import Group, Permission
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter
import csv
from django.http import HttpResponse
from dalmiadap import logger
log = logger.get_logger('admin_panel')


'''
@author(Kamal Agarwal)
Generic CSV Export
'''


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export Selected"


'''
@author(Kamal Agarwal)
Admin Filters
'''


class DesignationFilter(admin.SimpleListFilter):
    title = _('Designation')

    parameter_name = 'designation'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('designation__designation', 'designation__designation_name').distinct().order_by('designation__priority')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(designation__exact=self.value())


class BrandFilter(admin.SimpleListFilter):
    title = _('Brand')

    parameter_name = 'brand'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('brand__brand_id', 'brand__brand_name').distinct().order_by('brand__brand_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(brand__exact=self.value())


class RegionFilter(admin.SimpleListFilter):
    title = _('Region')

    parameter_name = 'region'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('region__region_id', 'region__region').distinct().order_by('region__region')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(region__exact=self.value())


class StateFilter(admin.SimpleListFilter):
    title = _('State')

    parameter_name = 'state'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('state__state_id', 'state__state').distinct().order_by('state__state')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(state__exact=self.value())


class DistrictFilter(admin.SimpleListFilter):
    title = _('District')

    parameter_name = 'district'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('district__district_id', 'district__district').distinct().order_by('district__district')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(district__exact=self.value())


class CityFilter(admin.SimpleListFilter):
    title = _('City')

    parameter_name = 'city'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('city__city_id', 'city__city').distinct().order_by('city__city')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(city__exact=self.value())


class BlockTalukaMandalFilter(admin.SimpleListFilter):
    title = "Block / Taluka / Mandal"
    parameter_name = "block_taluka_mandal"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('block_taluka_mandal__block_taluka_id', 'block_taluka_mandal__block_taluka').distinct().order_by('block_taluka_mandal__block_taluka')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(block_taluka_mandal__exact=self.value())


class StatusFilter(admin.SimpleListFilter):
    title = "Status"
    parameter_name = 'status'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('status', 'status').distinct().order_by('status')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(status__exact=self.value())


class VerificationFilter(admin.SimpleListFilter):
    title = "Verified status"
    parameter_name = "is_verified"

    def lookups(self, request, model_admin):
        return (
            ('unverified', 'Unverified'),
            ('only_counter_verified', 'Only Counter Verified'),
            ('verified', 'Complete Verified')
        )

    def queryset(self, request, queryset):
        if self.value() == 'verified':
            return queryset.filter(is_verified=True, is_sap_code_verified=True) if self.value() else queryset
        elif self.value() == 'unverified':
            return queryset.filter(is_verified=False, is_sap_code_verified=False) if self.value() else queryset
        elif self.value() == 'only_counter_verified':
            return queryset.filter(is_verified=True, is_sap_code_verified=False) if self.value() else queryset
        else:
            return queryset


class SOFilter(admin.SimpleListFilter):
    title = "Sales Officer"
    parameter_name = "so_code"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('so_code__emp_id', 'so_code__emp_name').distinct().order_by('so_code__emp_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(so_code__exact=self.value())


class ASMFilter(admin.SimpleListFilter):
    title = "ASM"
    parameter_name = "asm"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('asm__emp_id', 'asm__emp_name').distinct().order_by('asm__emp_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(asm__exact=self.value())


class RSMFilter(admin.SimpleListFilter):
    title = "RSM"
    parameter_name = "rsm"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('rsm__emp_id', 'rsm__emp_name').distinct().order_by('rsm__emp_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(rsm__exact=self.value())


class CoachFilter(admin.SimpleListFilter):
    title = "Coach"
    parameter_name = "coach"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('coach__emp_id', 'coach__emp_name').distinct().order_by('coach__emp_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(coach__exact=self.value())


class StateHeadFilter(admin.SimpleListFilter):
    title = "ZSM/State Head"
    parameter_name = "zsm_state_head"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('zsm_state_head__emp_id', 'zsm_state_head__emp_name').distinct().order_by('zsm_state_head__emp_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(zsm_state_head__exact=self.value())


class ClusterHeadFilter(admin.SimpleListFilter):
    title = "Cluster Head"
    parameter_name = "cluster_head"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('cluster_head__emp_id', 'cluster_head__emp_name').distinct().order_by('cluster_head__emp_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(cluster_head__exact=self.value())


'''
@author(Kamal Agarwal)
Model Admins
'''


class MyGroupAdmin(GroupAdmin):
    def get_form(self, request, obj=None, **kwargs):
        # Get form from original GroupAdmin.
        form = super(MyGroupAdmin, self).get_form(request, obj, **kwargs)
        if 'permissions' in form.base_fields:
            permissions = form.base_fields['permissions']
            permissions.queryset = permissions.queryset.exclude(
                content_type__app_label__in=['admin', 'contenttypes', 'sessions']).exclude(codename__startswith='delete_')  # Example
        return form


class UserCreationFormExtended(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(
            label=("E-mail"), max_length=75)
        # self.fields['designation'] = forms.ChoiceField(
        #     choices=[(r.designation, r.designation) for r in DesignationMaster.objects.all()])


class UserChangeFormExtended(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(UserChangeFormExtended, self).__init__(*args, **kwargs)


class MyUserAdmin(UserAdmin):

    date_hierarchy = 'updated_on'

    form = UserChangeFormExtended
    fieldsets = (
        (None, {'fields': ('username', )}),
        (_('Personal info'), {'fields': ('emp_id', 'first_name',
                                         'last_name', 'email', 'emp_phone_num', 'region', 'designation', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff',
                                       'groups', 'user_permissions')}),
        (_('Handled By'), {'fields': ('created_on',
                                      'created_by', 'updated_on', 'updated_by')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_form = UserCreationFormExtended
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('emp_id', 'email', 'emp_phone_num', 'first_name', 'last_name', 'region', 'designation', 'password1', 'password2', )
        }),
    )

    list_display = ('username', 'email', 'first_name',
                    'last_name', 'designation', 'is_staff',)
    list_filter = ('is_staff', 'is_active', 'groups', DesignationFilter, )
    readonly_fields = ('username', 'created_on', 'created_by',
                       'updated_on', 'updated_by', 'last_login', 'date_joined')
    search_fields = ('emp_id', 'emp_phone_num', 'first_name')
    ordering = ('designation__priority', '-updated_on',)

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in MyUserAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            qs = UserMaster.objects.exclude(
                designation__priority__gte=(request.user.designation.priority))
            if not request.user.has_perm('app.has_all_regions_access_regionmaster') and request.user.has_perm('app.has_current_region_access_only_regionmaster'):
                qs = qs.filter(region=request.user.region)
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in MyUserAdmin, Error msg: " + str(e))
            qs = UserMaster.objects.all()
        finally:
            return qs

    def get_actions(self, request):
        log.info("Enetered at get_actions in MyUserAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        actions = super().get_actions(request)
        try:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        except Exception as e:
            log.error(
                "Exception occured at get_actions in MyUserAdmin, Error msg: " + str(e))
        finally:
            return actions

    def get_form(self, request, obj=None, **kwargs):
        form = super(MyUserAdmin, self).get_form(request, obj, **kwargs)
        if 'user_permissions' in form.base_fields:
            user_permissions = form.base_fields['user_permissions']
            user_permissions.queryset = user_permissions.queryset.filter(codename__endswith='permission').exclude(content_type__app_label__in=['auth', 'admin', 'contenttypes', 'sessions'])
        return form

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        log.info("Enetered at save_model in MyUserAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        obj.added_by = request.user
        obj.updated_by = request.user.emp_name
        obj.updated_on = datetime.now()
        user_obj = UserMaster.objects.get(username=request.user.username)
        if form.cleaned_data.get('emp_phone_num'):
            obj.username = form.cleaned_data.get('emp_phone_num')
        if form.cleaned_data.get('first_name') and form.cleaned_data.get('last_name'):
            obj.emp_name = str(
                form.cleaned_data.get('first_name') + " " + form.cleaned_data.get('last_name')).strip()
        obj.is_superuser = False
        obj.is_staff = True
        if not obj.designation:
            if user_obj.designation__priority == 1:
                obj.designation = UserMaster.objects.filter(priority=2)
            elif user_obj.designation__priority == 2:
                obj.designation = UserMaster.objects.filter(priority=3)
        if not change:
            obj.created_by = request.user.emp_name

        log.info("Data to be eneterd/updated in UserMaster, Data are: emp_id::: " + str(obj.emp_id) + ", emp_name: " + str(obj.emp_name) +
                 ", emp_phone_num: " + str(obj.emp_phone_num) + ", designation: " + str(obj.designation) + ", region: " + str(obj.region) + ", username: " + str(obj.username))
        return super(MyUserAdmin, self).save_model(request, obj, form, change)

# Util method for OutletDetailsAdmin


class OutletDetailsUtil():
    def get_visible_outlets_result_set(self, request, calledFrom):
        log.info("Enetered at get_visible_outlets_result_set in OutletDetailsUtil, Called from " +
                 str(calledFrom) + ", Logged-in user emp_id: " + str(request.user.emp_id) + ".")
        try:
            qs = OutletDetails.objects.filter(
                status__contains=OutletDetails.ACTIVE)
            emp_id = UserMaster.objects.filter(
                username=request.user.username).first().emp_id
            if not request.user.is_superuser and not request.user.has_perm('app.has_all_regions_access_regionmaster') and request.user.has_perm('app.has_current_region_access_only_regionmaster'):
                qs = qs.filter(region=request.user.region)
            if not request.user.is_superuser and (request.user.has_perm('app.has_so_permission') or request.user.has_perm('app.has_asm_permission') or request.user.has_perm('app.has_rsm_permission') or request.user.has_perm('app.has_zsm_state_head_permission') or request.user.has_perm('app.has_coach_head_permission') or request.user.has_perm('app.has_cluster_head_permission')):
                ts = qs.filter(Q(so_code=emp_id) | Q(asm=emp_id) | Q(rsm=emp_id) | Q(
                    coach=emp_id) | Q(zsm_state_head=emp_id) | Q(cluster_head=emp_id))
                if ts:
                    qs = ts

        except Exception as e:
            log.error(
                "Exception occured at get_visible_outlets_result_set in OutletDetailsUtil, Error msg: " + str(e))
            qs = OutletDetails.objects.all()
        finally:
            return qs


class OutletDetailsAdmin(admin.ModelAdmin, ExportCsvMixin):
    date_hierarchy = 'updated_on'
    readonly_fields = ['is_verified', 'verified_by', 'is_sap_code_verified',
                       'sap_code_verified_by', 'created_on', 'created_by', 'updated_on', 'updated_by']
    list_per_page = 20
    search_fields = ('sap_code', 'counter_name', )
    list_filter = ('updated_on', StatusFilter, VerificationFilter, SOFilter, ASMFilter, RSMFilter, CoachFilter, StateHeadFilter, ClusterHeadFilter,
                   StateFilter, DistrictFilter, BlockTalukaMandalFilter)
    actions = ['update_so', 'update_asm', 'update_rsm', 'update_coach', 'update_zsm_state_head',
               'update_cluster_head', 'verify_outlet', 'verify_outlet_code', 'archive_outlets', 'export_as_csv']
    ordering = ('-updated_on',)

    def verification(self, obj):
        log.info("Enetered at verification in OutletDetailsAdmin.")
        if not obj.is_verified:
            return mark_safe("<a href={}>{}</a>".format(
                reverse('update_is_verified',
                        args=(obj.dap_id, )),
                "Verify outlet"))
        elif not obj.is_sap_code_verified and obj.sap_code != None and obj.sap_code != "":
            return mark_safe("<a href={}>{}</a>".format(
                reverse('update_is_sap_code_verified',
                        args=(obj.dap_id, )),
                "Verify outlet Code"))
        return format_html("<label><img src='/static/admin/img/icon-yes.svg' alt='True'></label>")
    verification.allowed_permissions = ('verify',)

    def get_queryset(self, request):
        return OutletDetailsUtil.get_visible_outlets_result_set(self, request, "OutletDetailsAdmin")

    def get_form(self, request, obj=None, **kwargs):
        return super(OutletDetailsAdmin, self).get_form(request, obj, **kwargs)

    def get_list_display(self, request):
        list_to_be_display = ['dap_id', 'sap_code', 'counter_name', 'so_code', 'asm', 'rsm',
                              'coach', 'zsm_state_head', 'cluster_head', 'state', 'district', 'block_taluka_mandal', 'status']
        if request.user.has_perm('app.verify_outletdetails'):
            list_to_be_display.append('verification')
        return tuple(list_to_be_display)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            OutletDetailsAdmin, self).get_readonly_fields(request, obj)
        if obj:
            readonly_fields_list = []
            if not request.user.has_perm('app.can_edit_basic_details_outletdetails'):
                readonly_fields_list.extend(
                    ['sap_code', 'counter_name', 'phone_number', 'is_cement_counter', 'counter_type', 'dob', 'comment'])
            if not request.user.has_perm('app.can_edit_geo_details_outletdetails'):
                readonly_fields_list.extend(
                    ['district', 'geo_location', 'block_taluka_mandal', 'state', 'region'])
            if not request.user.has_perm('app.can_edit_potential_outletdetails'):
                readonly_fields_list.extend(
                    ['unit', 'total_potential', 'wholesale_volume', 'retail_volume'])
            if obj.sap_code:
                readonly_fields_list.extend(
                    ['so_code', 'asm', 'rsm', 'coach', 'zsm_state_head', 'cluster_head', 'phone_number'])
            if not obj.sap_code and not request.user.has_perm('app.can_edit_sales_hierarchy_outletdetails'):
                readonly_fields_list.extend(
                    ['so_code', 'asm', 'rsm', 'coach', 'zsm_state_head', 'cluster_head'])
            if readonly_fields_list:
                return readonly_fields + readonly_fields_list
        return readonly_fields

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        log.info("Enetered at formfield_for_foreignkey in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            if db_field.name == "so_code":
                perm = Permission.objects.get(codename='has_so_permission')
                kwargs["queryset"] = UserMaster.objects.filter(
                    region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
            if db_field.name == "asm":
                perm = Permission.objects.get(codename='has_asm_permission')
                kwargs["queryset"] = UserMaster.objects.filter(
                    region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
            if db_field.name == "rsm":
                perm = Permission.objects.get(codename='has_rsm_permission')
                kwargs["queryset"] = UserMaster.objects.filter(
                    region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
            if db_field.name == "coach":
                perm = Permission.objects.get(
                    codename='has_coach_head_permission')
                kwargs["queryset"] = UserMaster.objects.filter(
                    region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
            if db_field.name == "zsm_state_head":
                perm = Permission.objects.get(
                    codename='has_zsm_state_head_permission')
                kwargs["queryset"] = UserMaster.objects.filter(
                    region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
            if db_field.name == "cluster_head":
                perm = Permission.objects.get(
                    codename='has_cluster_head_permission')
                kwargs["queryset"] = UserMaster.objects.filter(
                    region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
            # if db_field.name == "state":
            #     if not request.is_superuser and not request.has_perm('app.has_all_regions_access_regionmaster'):
            #         kwargs["queryset"] = StateMaster.objects.filter(
            #             region=request.user.region)
            # if db_field.name == "district":
            #     if not request.is_superuser and not request.has_perm('app.has_all_regions_access_regionmaster'):
            #         kwargs["queryset"] = DistrictMaster.objects.filter(
            #             state__region=request.user.region)
            # if db_field.name == "city":
            #     if not request.is_superuser and not request.has_perm('app.has_all_regions_access_regionmaster'):
            #         kwargs["queryset"] = CityMaster.objects.filter(
            #             district__state__region=request.user.region)
            # if db_field.name == "block_taluka_mandal":
            #     if not request.is_superuser and not request.has_perm('app.has_all_regions_access_regionmaster'):
            #         kwargs["queryset"] = BlockTalukaMaster.objects.filter(
            #             district__state__region=request.user.region)
        except Exception as ex:
            log.error(
                "Exception occured at formfield_for_foreignkey in OutletDetailsAdmin, Error msg: " + str(ex))
        finally:
            return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_verify_permission(self, request):
        """Does the user have the verify permission?"""
        opts = self.opts
        codename = get_permission_codename('verify', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))

    def has_bulk_verify_permission(self, request):
        """Does the user have the bulk_verify permission?"""
        opts = self.opts
        codename = get_permission_codename('bulk_verify', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))

    def has_bulk_update_permission(self, request):
        """Does the user have the bulk_update permission?"""
        opts = self.opts
        codename = get_permission_codename('bulk_update', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))

    def has_bulk_archive_permission(self, request):
        """Does the user have the bulk_archive permission?"""
        opts = self.opts
        codename = get_permission_codename('bulk_archive', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))

    def update_so(self, request, queryset):
        log.info("Enetered at update_so in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if 'apply' in request.POST:
            queryset.update(
                so_code=request.POST['selected_user'], updated_by=request.user, updated_on=datetime.now())
            self.message_user(
                request, "Successfully changed SO on {} Outlets".format(queryset.count()))
            log.info(
                str("Successfully changed SO on {} Outlets".format(queryset.count())))
            return HttpResponseRedirect(request.get_full_path())
        perm = Permission.objects.get(codename='has_so_permission')
        if not request.user.is_superuser:
            users = UserMaster.objects.filter(
                region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        else:
            users = UserMaster.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        return render(request, 'admin/oulet_bulk_update_intermediate.html', context={'counters': queryset, 'users': users, "current_selected_designation": "update_so"})
    update_so.short_description = "Update SO"
    update_so.allowed_permissions = ('bulk_update',)

    def update_asm(self, request, queryset):
        log.info("Enetered at update_asm in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if 'apply' in request.POST:
            queryset.update(
                asm=request.POST['selected_user'], updated_by=request.user, updated_on=datetime.now())
            self.message_user(
                request, "Successfully changed ASM on {} Outlets".format(queryset.count()))
            log.info(
                str("Successfully changed ASM on {} Outlets".format(queryset.count())))
            return HttpResponseRedirect(request.get_full_path())
        perm = Permission.objects.get(codename='has_asm_permission')
        if not request.user.is_superuser:
            users = UserMaster.objects.filter(
                region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        else:
            users = UserMaster.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        return render(request, 'admin/oulet_bulk_update_intermediate.html', context={'counters': queryset, 'users': users, "current_selected_designation": "update_asm"})
    update_asm.short_description = "Update ASM"
    update_asm.allowed_permissions = ('bulk_update',)

    def update_rsm(self, request, queryset):
        log.info("Enetered at update_rsm in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if 'apply' in request.POST:
            queryset.update(
                rsm=request.POST['selected_user'], updated_by=request.user, updated_on=datetime.now())
            self.message_user(
                request, "Successfully changed RSM on {} Outlets".format(queryset.count()))
            log.info(
                str("Successfully changed RSM on {} Outlets".format(queryset.count())))
            return HttpResponseRedirect(request.get_full_path())
        perm = Permission.objects.get(codename='has_rsm_permission')
        if not request.user.is_superuser:
            users = UserMaster.objects.filter(
                region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        else:
            users = UserMaster.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        return render(request, 'admin/oulet_bulk_update_intermediate.html', context={'counters': queryset, 'users': users, "current_selected_designation": "update_rsm"})
    update_rsm.short_description = "Update RSM"
    update_rsm.allowed_permissions = ('bulk_update',)

    def update_coach(self, request, queryset):
        log.info("Enetered at update_coach in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if 'apply' in request.POST:
            queryset.update(coach=request.POST['selected_user'],
                            updated_by=request.user, updated_on=datetime.now())
            self.message_user(
                request, "Successfully changed Coach on {} Outlets".format(queryset.count()))
            log.info(
                str("Successfully changed Coach on {} Outlets".format(queryset.count())))
            return HttpResponseRedirect(request.get_full_path())
        perm = Permission.objects.get(codename='has_coach_head_permission')
        if not request.user.is_superuser:
            users = UserMaster.objects.filter(
                region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        else:
            users = UserMaster.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        return render(request, 'admin/oulet_bulk_update_intermediate.html', context={'counters': queryset, 'users': users, "current_selected_designation": "update_coach"})
    update_coach.short_description = "Update Coach"
    update_coach.allowed_permissions = ('bulk_update',)

    def update_zsm_state_head(self, request, queryset):
        log.info("Enetered at update_zsm_state_head in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if 'apply' in request.POST:
            queryset.update(zsm_state_head=request.POST['selected_user'],
                            updated_by=request.user, updated_on=datetime.now())
            self.message_user(
                request, "Successfully changed ZSM/State Head on {} Outlets".format(queryset.count()))
            log.info(
                str("Successfully changed ZSM/State Head on {} Outlets".format(queryset.count())))
            return HttpResponseRedirect(request.get_full_path())
        perm = Permission.objects.get(codename='has_zsm_state_head_permission')
        if not request.user.is_superuser:
            users = UserMaster.objects.filter(
                region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        else:
            users = UserMaster.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        return render(request, 'admin/oulet_bulk_update_intermediate.html', context={'counters': queryset, 'users': users, "current_selected_designation": "update_zsm_state_head"})
    update_zsm_state_head.short_description = "Update ZSM/State Head"
    update_zsm_state_head.allowed_permissions = ('bulk_update',)

    def update_cluster_head(self, request, queryset):
        log.info("Enetered at update_cluster_head in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if 'apply' in request.POST:
            queryset.update(cluster_head=request.POST['selected_user'],
                            updated_by=request.user, updated_on=datetime.now())
            self.message_user(
                request, "Successfully changed Cluster Head on {} Outlets".format(queryset.count()))
            log.info(
                str("Successfully changed Cluster Head on {} Outlets".format(queryset.count())))
            return HttpResponseRedirect(request.get_full_path())
        perm = Permission.objects.get(codename='has_cluster_head_permission')
        if not request.user.is_superuser:
            users = UserMaster.objects.filter(
                region=request.user.region).filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        else:
            users = UserMaster.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)).distinct()
        return render(request, 'admin/oulet_bulk_update_intermediate.html', context={'counters': queryset, 'users': users, "current_selected_designation": "update_cluster_head"})
    update_cluster_head.short_description = "Update Cluster Head"
    update_cluster_head.allowed_permissions = ('bulk_update',)

    def verify_outlet(self, request, queryset):
        log.info("Enetered at verify_outlet in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            dapList = []
            for obj in queryset:
                dapList.append(obj.dap_id)
            canChangeSapIdsCount = OutletDetails.objects.filter(
                dap_id__in=dapList, is_verified=False).count()
            if canChangeSapIdsCount != queryset.count():
                self.message_user(
                    request, "Please select only not verified Outlets.", level=messages.ERROR)
                log.error("Selected Outlets are already verified, Selected Sap id list: [{}].".format(
                    str(canChangeSapIdsCount)))
            else:
                countObj = queryset.update(is_verified=True, verified_by=request.user,
                                           updated_by=request.user, updated_on=datetime.now())
                if countObj > 1:
                    self.message_user(
                        request, "{} Outlets verified.".format(countObj))
                elif countObj == 1:
                    self.message_user(
                        request, "{} Outlet verified.".format(countObj))
        except Exception as ex:
            log.error(
                "Exception occured while verify outlets, Error msg: " + str(ex))
            self.message_user(
                request, "Exception occured while verify outlets, Please try again later.", level=messages.ERROR)
        finally:
            return HttpResponseRedirect(request.get_full_path())
    verify_outlet.short_description = "Verify Outlets"
    verify_outlet.allowed_permissions = ('bulk_verify',)

    def verify_outlet_code(self, request, queryset):
        log.info("Enetered at verify_outlet_code in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            dapList = []
            for obj in queryset:
                dapList.append(obj.dap_id)
            canChangeSapIdsCount = OutletDetails.objects.filter(
                dap_id__in=dapList, is_sap_code_verified=False, sap_code__isnull=False).count()
            if canChangeSapIdsCount != queryset.count():
                self.message_user(
                    request, "Please select only those Outlets whose counter code exist and are not verified.", level=messages.ERROR)
                log.error("Selected Outlets are already verified or counter code does not exist, Selected Sap id list: [{}].".format(
                    str(canChangeSapIdsCount)))
            else:
                countObj = queryset.update(is_sap_code_verified=True, sap_code_verified_by=request.user,
                                           updated_by=request.user, updated_on=datetime.now())
                if countObj > 1:
                    self.message_user(
                        request, "{} Outlet codes verified.".format(countObj))
                elif countObj == 1:
                    self.message_user(
                        request, "{} Outlet code verified.".format(countObj))
        except Exception as ex:
            log.error(
                "Exception occured while verify outlet codes, Error msg: " + str(ex))
            self.message_user(
                request, "Exception occured while verify outlet codes, Please try again later.", level=messages.ERROR)
        finally:
            return HttpResponseRedirect(request.get_full_path())
    verify_outlet_code.short_description = "Verify Outlet Codes"
    verify_outlet_code.allowed_permissions = ('bulk_verify',)

    def archive_outlets(self, request, queryset):
        log.info("Enetered at archive_outlets in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            countObj = queryset.update(
                status=OutletDetails.ARCHIVE, updated_by=request.user, updated_on=datetime.now())
            if countObj > 1:
                self.message_user(
                    request, "{} Outlets archived.".format(countObj))
            elif countObj == 1:
                self.message_user(
                    request, "{} Outlet archived.".format(countObj))
        except Exception as ex:
            log.error(
                "Exception occured while archiving outlets, Error msg: " + str(ex))
            self.message_user(
                request, "Exception occured while archiving outlets, Please try again later.", level=messages.ERROR)
        finally:
            return HttpResponseRedirect(request.get_full_path())
    archive_outlets.short_description = "Archive Outlets"
    archive_outlets.allowed_permissions = ('bulk_archive',)

    # def render_change_form(self, request, context, *args, **kwargs):
    #     """We need to update the context to show the button."""
    #     # context.update({'show_save_and_copy': True})
    #     print('\n\n Change occured \n\n')
    #     return super().render_change_form(request, context, *args, **kwargs)

    def save_model(self, request, obj, form, change):
        log.info("Enetered at save_model in OutletDetailsAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        obj.updated_by = request.user
        obj.updated_on = datetime.now()
        if not change:
            obj.created_by = request.user
        return super(OutletDetailsAdmin, self).save_model(request, obj, form, change)


class OutletMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'updated_on'

    list_display = ('dap', 'no_of_employees', 'no_of_delivery_vans', 'average_cement_stock', 'next_generation_in_business',
                    'does_dealer_walk_the_market', 'no_of_sub_dealers', 'reputation_of_counter', 'created_on', 'created_by', 'updated_on', 'updated_by')

    list_filter = [('created_on', DateRangeFilter), 'reputation_of_counter', 'does_dealer_walk_the_market',
                   ('no_of_employees', RangeNumericFilter),
                   ('no_of_delivery_vans', RangeNumericFilter),
                   ('average_cement_stock', RangeNumericFilter)]

    readonly_fields = ['created_on',
                       'created_by', 'updated_on', 'updated_by']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in OutletMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            qs = (OutletMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__so_code=request.user.emp_id) |
                  OutletMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__asm=request.user.emp_id) |
                  OutletMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__rsm=request.user.emp_id) |
                  OutletMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__zsm_state_head=request.user.emp_id) |
                  OutletMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__cluster_head=request.user.emp_id))
            if not qs and request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = OutletMaster.objects.filter(
                    dap__status=OutletDetails.ACTIVE)
            if not qs and not request.user.has_perm('app.has_all_regions_access_regionmaster') and request.user.has_perm('app.has_current_region_access_only_regionmaster'):
                qs = OutletMaster.objects.filter(
                    dap__status=OutletDetails.ACTIVE, dap__region=request.user.region)
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in OutletMasterAdmin, Error msg: " + str(e))
            qs = OutletMaster.objects.filter(dap__status=OutletDetails.ACTIVE)
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            OutletMasterAdmin, self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['dap']
        return readonly_fields

    def save_model(self, request, obj, form, change):
        log.info("Enetered at save_model in OutletMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        obj.updated_by = request.user
        obj.updated_on = datetime.now()
        if not change:
            obj.created_by = request.user
        return super(OutletMasterAdmin, self).save_model(request, obj, form, change)


class GeoLocationMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'updated_on'

    list_display = ('dap', 'latitude', 'longitude', 'created_on', 'created_by', 'updated_on', 'updated_by')

    list_filter = ['created_on', 'updated_on']

    readonly_fields = ['created_on', 'created_by', 'updated_on', 'updated_by']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in GeoLocationMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            qs = (GeoLocationMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__so_code=request.user.emp_id) |
                  GeoLocationMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__asm=request.user.emp_id) |
                  GeoLocationMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__rsm=request.user.emp_id) |
                  GeoLocationMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__zsm_state_head=request.user.emp_id) |
                  GeoLocationMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__cluster_head=request.user.emp_id))
            if not qs and request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = GeoLocationMaster.objects.filter(
                    dap__status=OutletDetails.ACTIVE)
            if not qs and not request.user.has_perm('app.has_all_regions_access_regionmaster') and request.user.has_perm('app.has_current_region_access_only_regionmaster'):
                qs = GeoLocationMaster.objects.filter(
                    dap__status=OutletDetails.ACTIVE, dap__region=request.user.region)
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in GeoLocationMasterAdmin, Error msg: " + str(e))
            qs = GeoLocationMaster.objects.filter(dap__status=OutletDetails.ACTIVE)
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            GeoLocationMasterAdmin, self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['dap']
        return readonly_fields

    def save_model(self, request, obj, form, change):
        log.info("Enetered at save_model in GeoLocationMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        obj.updated_by = request.user
        obj.updated_on = datetime.now()
        if not change:
            obj.created_by = request.user
        if obj.latitude and obj.longitude:
            try:
                outletDetailsObject= OutletDetails.objects.get(dap_id=obj.dap_id)
                outletDetailsObject.geo_location = str(obj.latitude) + ", " + str(obj.longitude)
                outletDetailsObject.save()
            except Exception as ex:
                log.error("Exception occured while inserting geo_location in OutletDetails for DAP id: " + str(obj.dap_id) + ", Error msg: " + str(ex))
        return super(GeoLocationMasterAdmin, self).save_model(request, obj, form, change)


class BrandMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'updated_on'

    list_display = ('dap', 'brand', 'category', 'total_potential', 'total_wholesale_volume', 'total_retail_volume',
                    'wholesale_volume', 'retail_volume', 'opc', 'ppc', 'pcc', 'psc', 'created_on', 'created_by', 'updated_on', 'updated_by')

    list_filter = ['created_on', 'updated_on', BrandFilter]

    readonly_fields = ['created_on', 'created_by', 'updated_on', 'updated_by']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in BrandMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            qs = (BrandMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__so_code=request.user.emp_id) |
                  BrandMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__asm=request.user.emp_id) |
                  BrandMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__rsm=request.user.emp_id) |
                  BrandMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__zsm_state_head=request.user.emp_id) |
                  BrandMaster.objects.filter(dap__status=OutletDetails.ACTIVE, dap__cluster_head=request.user.emp_id))
            if not qs and request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = BrandMaster.objects.filter(
                    dap__status=OutletDetails.ACTIVE)
            if not qs and not request.user.has_perm('app.has_all_regions_access_regionmaster') and request.user.has_perm('app.has_current_region_access_only_regionmaster'):
                qs = BrandMaster.objects.filter(
                    dap__status=OutletDetails.ACTIVE, dap__region=request.user.region)
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in BrandMasterAdmin, Error msg: " + str(e))
            qs = BrandMaster.objects.filter(dap__status=OutletDetails.ACTIVE)
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            BrandMasterAdmin, self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['dap', 'brand_name']
        return readonly_fields

    def save_model(self, request, obj, form, change):
        log.info("Enetered at save_model in BrandMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        if obj.brand:
            obj.brand_name = BrandsList.objects.get(
                brand_id=obj.brand.brand_id).brand_name
        obj.updated_by = request.user
        obj.updated_on = datetime.now()
        if not change:
            obj.created_by = request.user
        return super(BrandMasterAdmin, self).save_model(request, obj, form, change)


class BrandsListAdmin(admin.ModelAdmin, ExportCsvMixin):

    # date_hierarchy = 'updated_on'

    # list_display = ('dap', 'brand', 'category', 'total_potential', 'total_wholesale_volume', 'total_retail_volume',
    #                 'wholesale_volume', 'retail_volume', 'opc', 'ppc', 'pcc', 'psc', 'created_on', 'created_by', 'updated_on', 'updated_by')

    # list_filter = ['created_on', 'updated_on', BrandFilter]

    # readonly_fields = ['created_on', 'created_by', 'updated_on', 'updated_by']

    ordering = ('brand_name',)

    actions = ["export_as_csv"]

    # def get_queryset(self, request):
    #     log.info("Enetered at get_queryset in BrandsListAdmin, Logged-in user emp_id: " +
    #              str(request.user.emp_id) + ".")
    #     try:
    #         qs = (BrandsList.objects.filter(dap__status=OutletDetails.ACTIVE, dap__so_code=request.user.emp_id) |
    #               BrandsList.objects.filter(dap__status=OutletDetails.ACTIVE, dap__asm=request.user.emp_id) |
    #               BrandsList.objects.filter(dap__status=OutletDetails.ACTIVE, dap__rsm=request.user.emp_id) |
    #               BrandsList.objects.filter(dap__status=OutletDetails.ACTIVE, dap__zsm_state_head=request.user.emp_id) |
    #               BrandsList.objects.filter(dap__status=OutletDetails.ACTIVE, dap__cluster_head=request.user.emp_id))
    #         if not qs and request.user.has_perm('app.has_all_regions_access_regionmaster'):
    #             qs = BrandsList.objects.filter(
    #                 dap__status=OutletDetails.ACTIVE)
    #         if not qs and not request.user.has_perm('app.has_all_regions_access_regionmaster') and request.user.has_perm('app.has_current_region_access_only_regionmaster'):
    #             qs = BrandsList.objects.filter(
    #                 dap__status=OutletDetails.ACTIVE, dap__region=request.user.region)
    #     except Exception as e:
    #         log.error(
    #             "Exception occured at get_queryset in BrandsListAdmin, Error msg: " + str(e))
    #         qs = BrandsList.objects.filter(dap__status=OutletDetails.ACTIVE)
    #     finally:
    #         return qs

    # def get_readonly_fields(self, request, obj=None):
    #     readonly_fields = super(
    #         BrandsListAdmin, self).get_readonly_fields(request, obj)
    #     if obj:  # editing an existing object
    #         return readonly_fields + ['dap', 'brand_name']
    #     return readonly_fields

    # def save_model(self, request, obj, form, change):
    #     log.info("Enetered at save_model in BrandsListAdmin, Logged-in user emp_id: " +
    #              str(request.user.emp_id) + ".")
    #     if obj.brand:
    #         obj.brand_name = BrandsList.objects.get(
    #             brand_id=obj.brand.brand_id).brand_name
    #     obj.updated_by = request.user
    #     obj.updated_on = datetime.now()
    #     if not change:
    #         obj.created_by = request.user
    #     return super(BrandsListAdmin, self).save_model(request, obj, form, change)


class RegionMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('region_id', 'region', 'created_on')

    list_filter = ['created_on']

    readonly_fields = ['created_on']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in RegionMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            if request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = RegionMaster.objects.all()
            else:
                qs = RegionMaster.objects.filter(
                    region=request.user.region).all()
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in RegionMasterAdmin, Error msg: " + str(e))
            qs = RegionMaster.objects.all()
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            RegionMasterAdmin, self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['region_id']
        return readonly_fields


class StateMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('state_id', 'state', 'region', 'created_on')

    list_filter = ['created_on', RegionFilter]

    readonly_fields = ['created_on']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in StateMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            if request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = StateMaster.objects.all()
            else:
                qs = StateMaster.objects.filter(
                    region=request.user.region).all()
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in StateMasterAdmin, Error msg: " + str(e))
            qs = StateMaster.objects.all()
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            StateMasterAdmin, self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['state_id']
        return readonly_fields


class DistrictMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('district_id', 'district', 'state', 'created_on')

    list_filter = ['created_on', StateFilter]

    readonly_fields = ['created_on']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in DistrictMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            if request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = DistrictMaster.objects.all()
            else:
                qs = DistrictMaster.objects.filter(
                    state__region=request.user.region).all()
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in DistrictMasterAdmin, Error msg: " + str(e))
            qs = DistrictMaster.objects.all()
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(DistrictMasterAdmin,
                                self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['district_id']
        return readonly_fields


class CityMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('city_id', 'city', 'district', 'created_on')

    list_filter = ['created_on', DistrictFilter]

    readonly_fields = ['created_on']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in CityMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            if request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = CityMaster.objects.all()
            else:
                qs = CityMaster.objects.filter(
                    district__state__region=request.user.region).all()
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in CityMasterAdmin, Error msg: " + str(e))
            qs = CityMaster.objects.all()
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(
            CityMasterAdmin, self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['city_id']
        return readonly_fields


class BlockTalukaMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('block_taluka_id', 'block_taluka',
                    'district', 'created_on')

    list_filter = ['created_on', DistrictFilter]

    readonly_fields = ['created_on']

    ordering = ('-created_on',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in BlockTalukaMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            if request.user.has_perm('app.has_all_regions_access_regionmaster'):
                qs = BlockTalukaMaster.objects.all()
            else:
                qs = BlockTalukaMaster.objects.filter(
                    district__state__region=request.user.region).all()
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in BlockTalukaMasterAdmin, Error msg: " + str(e))
            qs = BlockTalukaMaster.objects.all()
        finally:
            return qs

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(BlockTalukaMasterAdmin,
                                self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['block_taluka_id']
        return readonly_fields


class DesignationMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('designation', 'designation_name',
                    'priority', 'created_on')

    list_filter = ['created_on', 'priority']

    readonly_fields = ['created_on']

    ordering = ('priority',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in DesignationMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        return DesignationMaster.objects.all()

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(DesignationMasterAdmin,
                                self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['designation', 'designation_name']
        return readonly_fields


# Authentications and permissions register
admin.site.register(DesignationMaster, DesignationMasterAdmin)
admin.site.unregister(Group)
admin.site.register(Group, MyGroupAdmin)

# Location registers
admin.site.register(RegionMaster, RegionMasterAdmin)
admin.site.register(StateMaster, StateMasterAdmin)
admin.site.register(DistrictMaster, DistrictMasterAdmin)
admin.site.register(CityMaster, CityMasterAdmin)
admin.site.register(BlockTalukaMaster, BlockTalukaMasterAdmin)

# DAP registers
admin.site.register(UserMaster, MyUserAdmin)
admin.site.register(OutletDetails, OutletDetailsAdmin)
admin.site.register(OutletMaster, OutletMasterAdmin)
admin.site.register(BrandMaster, BrandMasterAdmin)
admin.site.register(BrandsList, BrandsListAdmin)
admin.site.register(GeoLocationMaster, GeoLocationMasterAdmin)
