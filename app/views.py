from django.shortcuts import render, render_to_response
from .forms import *
from .models import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.http import JsonResponse
from django.utils import timezone
from datetime import datetime
from .common_response import *
import threading
import xlsxwriter
import os
import json
from django.contrib.auth.models import Group, Permission
from django.db.models import Q
from django.utils.html import mark_safe, format_html
from django.urls import reverse
from django.http import FileResponse, HttpResponseRedirect
import traceback
from dalmiadap import logger
log = logger.get_logger('app')


@csrf_exempt
def health_check_api(request):
    log.info("health_check_api, Request " + str(request))
    data = {
        "info": "Health checked successfully.",
        "status": True,
        "status_code": 200,
        "success": "SUCCESS",
        "msg": "Health checked successfully."
    }
    return JsonResponse(data)


@csrf_exempt
def update_is_verified(request, dap_id):
    log.info("In update_is_verified, Request " + str(request))
    try:
        if not request.user.has_perm('app.verify_outletdetails'):
            log.error("Permission Denied to update OutletDetails.is_verified for dap_id: " +
                      str(dap_id) + ", Logged-in User Emp Id: " + str(request.user.emp_id))
        else:
            outletDetailsObject = OutletDetails.objects.get(dap_id=dap_id)
            outletDetailsObject.is_verified = True
            outletDetailsObject.verified_by = request.user
            outletDetailsObject.updated_on = timezone.now()
            outletDetailsObject.updated_by = request.user
            outletDetailsObject.save()
            log.info("OutletDetails.is_verified for dap_id: " +
                     str(dap_id) + " has been marked 'True' successfully.")
    except Exception as ex:
        log.error("Exception occured while updating OutletDetails.is_verified for dap_id: " +
                  str(dap_id) + ", Error msg: " + str(ex))
    return HttpResponseRedirect(reverse('admin:app_outletdetails_changelist'))


@csrf_exempt
def update_is_sap_code_verified(request, dap_id):
    log.info("In update_is_sap_code_verified, Request " + str(request))
    try:
        if not request.user.has_perm('app.verify_outletdetails'):
            log.error("Permission Denied to update OutletDetails.is_sap_code_verified for dap_id: " +
                      str(dap_id) + ", Logged-in User Emp Id: " + str(request.user.emp_id))
        else:
            outletDetailsObject = OutletDetails.objects.get(dap_id=dap_id)
            outletDetailsObject.is_sap_code_verified = True
            outletDetailsObject.sap_code_verified_by = request.user
            outletDetailsObject.updated_on = timezone.now()
            outletDetailsObject.updated_by = request.user
            outletDetailsObject.save()
            log.info("OutletDetails.is_sap_code_verified for dap_id: " +
                     str(dap_id) + " has been marked 'True' successfully.")
    except Exception as ex:
        log.error("Exception occured while updating OutletDetails.is_sap_code_verified for dap_id: " +
                  str(dap_id) + ", Error msg: " + str(ex))
    return HttpResponseRedirect(reverse('admin:app_outletdetails_changelist'))


@csrf_exempt
def home(request):
    log.info("404 Not Found, Request " + str(request))
    return render(request, 'home.html')


def show_page_1(request, so_code, lat_long=""):
    if so_code:
        try:
            user_object = UserMaster.objects.get(
                emp_id=so_code)
            if not user_object.has_perm('app.has_so_permission'):
                print("EMP Id : " + str(so_code) + " is not a Sales Officer.")
                log.error("EMP Id : " + str(so_code) +
                          " is not a Sales Officer.")
                return redirect('/home/')
        except:
            log.error("No data found for so_code: " + str(so_code))
            return redirect('/home/')
        form1 = Type1Page1Form()
        form1.fields['so_code'].initial = user_object.emp_id

        states_qs = StateMaster.objects.filter(
            region=user_object.region.region_id).order_by('state')
        states = [("", "--SELECT--")]
        for x in states_qs:
            states.append((x.state_id, x.state))
        form1.fields['state'].choices = states

        district_qs = DistrictMaster.objects.filter(
            state__region=user_object.region.region_id).order_by('district')
        districts = [("", "--SELECT--")]
        for x in district_qs:
            districts.append((x.district_id, x.district))
        form1.fields['district'].choices = districts

        city_qs = CityMaster.objects.filter(
            district__state__region=user_object.region.region_id).order_by('city')
        cities = [("", "--SELECT--")]
        for x in city_qs:
            cities.append((x.city_id, x.city))
        form1.fields['city'].choices = cities

        block_taluka_qs = BlockTalukaMaster.objects.filter(
            district__state__region=user_object.region.region_id).order_by('block_taluka')
        block_talukas = [("", "--SELECT--")]
        for x in block_taluka_qs:
            block_talukas.append((x.block_taluka_id, x.block_taluka))
        form1.fields['block_taluka_mandal'].choices = block_talukas

        if lat_long:
            form1.fields['geo_location'].initial = lat_long
        outletDetails = OutletDetails.objects.filter(
            so_code_id=user_object.emp_id).all()
        # form1.fields['outlet_name'].choices = [
        #     (x.dap_id, x.counter_name) for x in outletDetails]
        # form1.fields['dap_id'].choices = [
        #     (x.dap_id, x.dap_id) for x in outletDetails]
        outlet_id_list = [('', '-- Select Id --')]
        for x in outletDetails:
            option = (x.dap_id, x.sap_code)
            outlet_id_list.append(option)
        outlet_name_list = [('', '-- Select Brand --')]
        for x in outletDetails:
            option = (x.dap_id, x.counter_name)
            outlet_name_list.append(option)
        log.info("Successfully returned to page-1 with so_code: " + str(so_code))
        return render(request, 'app1page1.html', {'form': form1, 'outlet_id_list': outlet_id_list, 'outlet_name_list': outlet_name_list})
    else:
        log.error("No so_code found in request.")
        return redirect('/home/')


@csrf_exempt
def show_page_1_with_so_code(request, so_code):
    log.info("show_page_1_with_so_code of views, So code : " +
             str(so_code) + ", Request " + str(request))
    return show_page_1(request, so_code)


@csrf_exempt
def show_page_1_with_so_code_lat(request, so_code, lat):
    log.info("show_page_1_with_so_code_lat of views, So code : " +
             str(so_code) + ", Latitude : " + str(lat) + ", Request " + str(request))
    return redirect('/page-1/' + so_code + '/')


@csrf_exempt
def show_page_1_with_so_code_lat_long(request, so_code, lat, long):
    log.info("show_page_1_with_so_code_lat of views, So code : " + str(so_code) +
             ", Latitude : " + str(lat) + ", Longitude : " + str(long) + ", Request " + str(request))
    try:
        latitude = float(lat)
        longitude = float(long)
        lat_long = str(latitude) + ", " + str(longitude)
        return show_page_1(request, so_code, lat_long)
    except:
        log.error("Latitude and longitude is/are not of float type.")
        return redirect('/page-1/' + so_code + '/')


@csrf_exempt
def edit_page_1_with_latlong(request, so_code, dap_id, lat_long):
    return edit_page_1(request, so_code, dap_id, lat_long)


@csrf_exempt
def edit_page_1_without_latlong(request, so_code, dap_id):
    return edit_page_1(request, so_code, dap_id)


def edit_page_1(request, so_code, dap_id, lat_long=""):
    if so_code and dap_id:
        try:
            user_object = UserMaster.objects.get(
                emp_id=so_code)
        except:
            log.error("No data found for so_code: " + str(so_code))
            return redirect('/home/')
        form1 = Type1Page1Form()
        form1.fields['so_code'].initial = user_object.emp_id
        form1.fields['dap_id'].initial = dap_id

        states_qs = StateMaster.objects.filter(
            region=user_object.region.region_id).order_by('state')
        states = [("", "--SELECT--")]
        for x in states_qs:
            states.append((x.state_id, x.state))
        form1.fields['state'].choices = states

        district_qs = DistrictMaster.objects.filter(
            state__region=user_object.region.region_id).order_by('district')
        districts = [("", "--SELECT--")]
        for x in district_qs:
            districts.append((x.district_id, x.district))
        form1.fields['district'].choices = districts

        city_qs = CityMaster.objects.filter(
            district__state__region=user_object.region.region_id).order_by('city')
        cities = [("", "--SELECT--")]
        for x in city_qs:
            cities.append((x.city_id, x.city))
        form1.fields['city'].choices = cities

        block_taluka_qs = BlockTalukaMaster.objects.filter(
            district__state__region=user_object.region.region_id).order_by('block_taluka')
        block_talukas = [("", "--SELECT--")]
        for x in block_taluka_qs:
            block_talukas.append((x.block_taluka_id, x.block_taluka))
        form1.fields['block_taluka_mandal'].choices = block_talukas

        if lat_long:
            form1.fields['geo_location'].initial = lat_long
        outletDetails = OutletDetails.objects.filter(
            so_code_id=user_object.emp_id).all()
        # form1.fields['outlet_name'].choices = [
        #     (x.dap_id, x.counter_name) for x in outletDetails]
        # form1.fields['dap_id'].choices = [
        #     (x.dap_id, x.dap_id) for x in outletDetails]
        outlet_id_list = [('', '-- Select Id --')]
        for x in outletDetails:
            option = (x.dap_id, x.sap_code)
            outlet_id_list.append(option)
        outlet_name_list = [('', '-- Select Brand --')]
        for x in outletDetails:
            option = (x.dap_id, x.counter_name)
            outlet_name_list.append(option)
        log.info("Successfully returned to page-1 with so_code: " + str(so_code))
        return render(request, 'app1page1.html', {'form': form1, 'outlet_id_list': outlet_id_list, 'outlet_name_list': outlet_name_list})
    else:
        log.error("No so_code found in request.")
        return redirect('/home/')


@csrf_exempt
def show_page_2(request, dap_id):
    log.info("show_page_2 of views, Sap id : " +
             str(dap_id) + ", Request " + str(request))
    if dap_id:
        form1 = Type1Page2Form()
        form1.fields["dap_id"].initial = dap_id
        try:
            outlet_details_object = OutletDetails.objects.filter(
                dap_id=dap_id).order_by('-created_on')[:1]
            for outlet_details in outlet_details_object:
                if outlet_details.so_code:
                    if outlet_details.so_code.has_perm('app.has_so_permission'):
                        form1.fields["so_code"].initial = outlet_details.so_code_id
                elif outlet_details.asm:
                    if outlet_details.asm.has_perm('app.has_so_permission'):
                        form1.fields["so_code"].initial = outlet_details.asm_id
                elif outlet_details.rsm:
                    if outlet_details.rsm.has_perm('app.has_so_permission'):
                        form1.fields["so_code"].initial = outlet_details.rsm_id
                elif outlet_details.zsm_state_head:
                    if outlet_details.zsm_state_head.has_perm('app.has_so_permission'):
                        form1.fields["so_code"].initial = outlet_details.zsm_state_head_id
                elif outlet_details.coach:
                    if outlet_details.coach.has_perm('app.has_so_permission'):
                        form1.fields["so_code"].initial = outlet_details.coach_id
                elif outlet_details.cluster_head:
                    if outlet_details.cluster_head.has_perm('app.has_so_permission'):
                        form1.fields["so_code"].initial = outlet_details.cluster_head_id
                form1.fields['is_a_cement_counter'].initial = outlet_details.is_cement_counter
                if outlet_details.is_cement_counter == 'no':
                    form1.fields['is_a_cnf_cfa'].widget.attrs['disabled'] = True
                    form1.fields['is_a_cnf_cfa_radio'].widget.attrs['disabled'] = True
                    form1.fields['is_a_sp'].widget.attrs['disabled'] = True
                    form1.fields['is_a_sp_radio'].widget.attrs['disabled'] = True
                    form1.fields['is_a_transporter'].widget.attrs['disabled'] = True
                    form1.fields['is_a_transporter_radio'].widget.attrs['disabled'] = True
                    form1.fields['is_a_indenting_agent'].widget.attrs['disabled'] = True
                    form1.fields['is_a_indenting_agent_radio'].widget.attrs['disabled'] = True
                    form1.fields['reputation_of_counter'].widget.attrs['disabled'] = True
            outlet_master_object = OutletMaster.objects.filter(
                dap_id=dap_id).order_by('-created_on')[:1]
            for outlet_mater in outlet_master_object:
                if outlet_mater.non_cement_turnover != '0':
                    form1.fields["non_cement_turnover"].initial = outlet_mater.non_cement_turnover
                    form1.fields["business_1"].initial = outlet_mater.business_1
                    form1.fields["average_monthly_sales_1"].initial = outlet_mater.average_monthly_sales_1
                    form1.fields["business_2"].initial = outlet_mater.business_2
                    form1.fields["average_monthly_sales_2"].initial = outlet_mater.average_monthly_sales_2
                else:
                    form1.fields['any_non_cement_business'].initial = 'no'
                    form1.fields['non_cement_turnover'].widget.attrs['readonly'] = True
                    form1.fields['business_1'].widget.attrs['readonly'] = True
                    form1.fields['average_monthly_sales_1'].widget.attrs['readonly'] = True
                    form1.fields['business_2'].widget.attrs['readonly'] = True
                    form1.fields['average_monthly_sales_2'].widget.attrs['readonly'] = True
                if outlet_mater.cnf_cfa_brand_names == '':
                    form1.fields["is_a_cnf_cfa_radio"].initial = 'no'
                    form1.fields['is_a_cnf_cfa'].initial = ''
                    form1.fields['is_a_cnf_cfa'].widget.attrs['readonly'] = True
                else:
                    form1.fields["is_a_cnf_cfa"].initial = outlet_mater.cnf_cfa_brand_names
                if outlet_mater.sp_brand_names == '':
                    form1.fields["is_a_sp_radio"].initial = 'no'
                    form1.fields['is_a_sp'].initial = ''
                    form1.fields['is_a_sp'].widget.attrs['readonly'] = True
                else:
                    form1.fields["is_a_sp"].initial = outlet_mater.sp_brand_names
                if outlet_mater.transporter_brand_names == '':
                    form1.fields["is_a_transporter_radio"].initial = 'no'
                    form1.fields['is_a_transporter'].initial = ''
                    form1.fields['is_a_transporter'].widget.attrs['readonly'] = True
                else:
                    form1.fields["is_a_transporter"].initial = outlet_mater.transporter_brand_names
                if outlet_mater.indenting_agents_brand_names == '':
                    form1.fields["is_a_indenting_agent_radio"].initial = 'no'
                    form1.fields['is_a_indenting_agent'].initial = ''
                    form1.fields['is_a_indenting_agent'].widget.attrs['readonly'] = True
                else:
                    form1.fields["is_a_indenting_agent"].initial = outlet_mater.indenting_agents_brand_names
                if outlet_mater.reputation_of_counter:
                    form1.fields["reputation_of_counter"].initial = outlet_mater.reputation_of_counter
        except Exception as exception:
            log.error(
                "Exception occured in show_page_2 of views, exception msg: " + str(exception) + ".")
            return redirect('/home/')
        brandList = BrandsList.objects.all().order_by('brand_name')
        brand_name_list = [('', '-- Select Brand --')]
        for x in brandList:
            option = (x.brand_name, x.brand_name)
            brand_name_list.append(option)
        log.info("Successfully returned to page-2.")
        return render(request, 'app1page2.html', {'form': form1, 'brand_name_list': brand_name_list})
    else:
        log.error("No dap_id found in request.")
        return redirect('/home/')


def validate_counter_by_dap_id(request):
    log.info("validate_counter_by_dap_id of views, Request " + str(request))
    counter_qs = OutletDetails.objects.filter(
        dap_id=request.GET['dap_id']).prefetch_related('outletmaster_set').all()
    # counter_qs = OutletMaster.objects.select_related(
    #     'dap').filter(dap_id=request.GET['dap_id']).all()
    counters = []
    for counter in counter_qs:
        outlet_master_data = [{
            'no_of_employees': outletmaster.no_of_employees,
            'no_of_delivery_vans': outletmaster.no_of_delivery_vans,
            'average_cement_stock': outletmaster.average_cement_stock,
            'next_generation_in_business': outletmaster.next_generation_in_business,
            'does_dealer_walk_the_market': outletmaster.does_dealer_walk_the_market,
            'personal_investment_in_business': outletmaster.personal_investment_in_business,
            'no_of_sub_dealers': outletmaster.no_of_sub_dealers,
            'non_cement_turnover': outletmaster.non_cement_turnover,
            'business_1': outletmaster.business_1,
            'average_monthly_sales_1': outletmaster.average_monthly_sales_1,
            'business_2': outletmaster.business_2,
            'average_monthly_sales_2': outletmaster.average_monthly_sales_2,
            'cnf_cfa_brand_names': outletmaster.cnf_cfa_brand_names,
            'sp_brand_names': outletmaster.sp_brand_names,
            'transporter_brand_names': outletmaster.transporter_brand_names,
            'indenting_agents_brand_names': outletmaster.indenting_agents_brand_names,
            'reputation_of_counter': outletmaster.reputation_of_counter,
            'created_on': outletmaster.created_on
        } for outletmaster in counter.outletmaster_set.all().order_by('-created_on')[:1]]
        counters.append({
            'dap_id': counter.dap_id,
            'counter_name': counter.counter_name,
            'sap_code': counter.sap_code,
            'state': counter.state.state_id if counter.state else '',
            'district': counter.district.district_id if counter.district else '',
            'city': counter.city.city_id if counter.city else '',
            'block_taluka_mandal': counter.block_taluka_mandal.block_taluka_id if counter.block_taluka_mandal else '',
            'phone_number': counter.phone_number,
            'dob': counter.dob,
            'comment': counter.comment,
            'geo_location': counter.geo_location,
            'is_cement_counter': counter.is_cement_counter,
            'outlet_master': outlet_master_data
        })
    data = {
        'counter_data': counters
    }
    log.info("Successfully returned with counters data: " + str(data))
    return JsonResponse(data)


def fetch_outlet_data_by_dap_id(dap_id):
    log.info("fetch_outlet_data_by_dap_id of views, Sap id " + str(dap_id))
    counter_qs = OutletDetails.objects.filter(
        dap_id=dap_id).prefetch_related('outletmaster_set').all()
    # counter_qs = OutletMaster.objects.select_related(
    #     'dap').filter(dap_id=request.GET['dap_id']).all()
    counters = []
    for counter in counter_qs:
        outlet_master_data = [{
            'no_of_employees': outletmaster.no_of_employees,
            'no_of_delivery_vans': outletmaster.no_of_delivery_vans,
            'average_cement_stock': outletmaster.average_cement_stock,
            'next_generation_in_business': outletmaster.next_generation_in_business,
            'does_dealer_walk_the_market': outletmaster.does_dealer_walk_the_market,
            'personal_investment_in_business': outletmaster.personal_investment_in_business,
            'no_of_sub_dealers': outletmaster.no_of_sub_dealers,
            'non_cement_turnover': outletmaster.non_cement_turnover,
            'business_1': outletmaster.business_1,
            'average_monthly_sales_1': outletmaster.average_monthly_sales_1,
            'business_2': outletmaster.business_2,
            'average_monthly_sales_2': outletmaster.average_monthly_sales_2,
            'cnf_cfa_brand_names': outletmaster.cnf_cfa_brand_names,
            'sp_brand_names': outletmaster.sp_brand_names,
            'transporter_brand_names': outletmaster.transporter_brand_names,
            'indenting_agents_brand_names': outletmaster.indenting_agents_brand_names,
            'reputation_of_counter': outletmaster.reputation_of_counter,
            'created_on': outletmaster.created_on
        } for outletmaster in counter.outletmaster_set.all().order_by('-created_on')[:1]]
        counters.append({
            'dap_id': counter.dap_id,
            'so_code': counter.so_code.emp_id,
            'counter_name': counter.counter_name,
            'sap_code': counter.sap_code,
            'state': counter.state,
            'district': counter.district,
            'city': counter.city,
            'block_taluka_mandal': counter.block_taluka_mandal,
            'phone_number': counter.phone_number,
            'geo_location': counter.geo_location,
            'is_cement_counter': counter.is_cement_counter,
            'dob': counter.dob,
            'comment': counter.comment,
            'outlet_master': outlet_master_data
        })
    log.info("Successfully returned with counters data: " + str(counters))
    return counters


@csrf_exempt
def save_page_1(request):
    log.info("save_page_1 of views, Request " + str(request))
    current_timestamp = timezone.now()
    # import pdb
    # pdb.set_trace()
    form = Type1Page1Form(request.POST or None)
    if form.is_valid():
        # UserMaster.objects.create(emp_id=form.cleaned_data['so_code'], emp_name=form.cleaned_data['counter_name'])
        user_object = UserMaster.objects.get(
            emp_id=form.cleaned_data['so_code'])
        geoLocation = None
        if form.cleaned_data['geo_location']:
            try:
                complete_location = str(
                    form.cleaned_data['geo_location']).split(",")
                latitude = float(str(complete_location[0]).strip()) if len(
                    complete_location) > 0 else None
                longitude = float(str(complete_location[1]).strip()) if len(
                    complete_location) > 1 else None
                if latitude and longitude and int(latitude) != 0 and int(longitude) != 0:
                    geoLocation = str(latitude) + ", " + str(longitude)
                    GeoLocationMaster.objects.create(
                        dap_id=form.cleaned_data['dap_id'], latitude=latitude, longitude=longitude, updated_by=user_object, created_by=user_object)
                    log.info("GEO location inserted for DAP id: " + str(form.cleaned_data['dap_id']) + ", Geo Location enetered are latitue: " + str(
                        latitude) + " and longitude: " + str(longitude) + ".")
                else:
                    log.error("Incorrect Geo Location for DAP id: " + str(
                        form.cleaned_data['dap_id']) + ", Geo Location enetered: " + str(form.cleaned_data['geo_location']) + ".")
            except Exception as ex:
                log.error("Exception occured while inserting geo location in GeoLoationMaster for dap id: " +
                          str(form.cleaned_data['dap_id']) + ", Error msg: " + str(ex))
        try:
            counter_object = OutletDetails.objects.get(
                dap_id=form.cleaned_data['dap_id'])
            if form.cleaned_data['state']:
                counter_object.state = StateMaster.objects.get(
                    state_id=form.cleaned_data['state'])
            if form.cleaned_data['district']:
                counter_object.district = DistrictMaster.objects.get(
                    district_id=form.cleaned_data['district'])
            if form.cleaned_data['city']:
                counter_object.city = CityMaster.objects.get(
                    city_id=form.cleaned_data['city'])
            if form.cleaned_data['block_taluka_mandal']:
                counter_object.block_taluka_mandal = BlockTalukaMaster.objects.get(
                    block_taluka_id=form.cleaned_data['block_taluka_mandal'])
            counter_object.phone_number = form.cleaned_data['phone_number']
            counter_object.geo_location = geoLocation
            counter_object.is_cement_counter = form.cleaned_data['is_a_cement_counter']
            counter_object.dob = form.cleaned_data['dob']
            counter_object.updated_on = current_timestamp
            counter_object.updated_by = user_object
            counter_object.save()
            log.info("Couter data updated in OutletDetails for dap id: " +
                     str(form.cleaned_data['dap_id']))
        except:
            counter_object = OutletDetails.objects.create(dap_id=form.cleaned_data['dap_id'],
                                                          state=form.cleaned_data['state'],
                                                          district=form.cleaned_data['district'],
                                                          city=form.cleaned_data['city'],
                                                          phone_number=form.cleaned_data['phone_number'],
                                                          geo_location=geoLocation,
                                                          block_taluka_mandal=form.cleaned_data[
                                                              'block_taluka_mandal'],
                                                          dob=form.cleaned_data['dob'],
                                                          is_cement_counter=form.cleaned_data['is_a_cement_counter'],
                                                          so_code=user_object,
                                                          updated_by=user_object,
                                                          created_by=user_object)
            log.info("Couter data created in OutletDetails for dap id: " +
                     str(form.cleaned_data['dap_id']))
        form1 = Type1Page2Form()
        try:
            outlet_master_object = OutletMaster.objects.filter(
                dap_id=counter_object.dap_id).order_by('-created_on')[:1]
            for outlet_mater in outlet_master_object:
                if outlet_mater.non_cement_turnover != '0':
                    form1.fields["non_cement_turnover"].initial = outlet_mater.non_cement_turnover
                    form1.fields["business_1"].initial = outlet_mater.business_1
                    form1.fields["average_monthly_sales_1"].initial = outlet_mater.average_monthly_sales_1
                    form1.fields["business_2"].initial = outlet_mater.business_2
                    form1.fields["average_monthly_sales_2"].initial = outlet_mater.average_monthly_sales_2
                else:
                    form1.fields['any_non_cement_business'].initial = 'no'
                    form1.fields['non_cement_turnover'].widget.attrs['readonly'] = True
                    form1.fields['business_1'].widget.attrs['readonly'] = True
                    form1.fields['average_monthly_sales_1'].widget.attrs['readonly'] = True
                    form1.fields['business_2'].widget.attrs['readonly'] = True
                    form1.fields['average_monthly_sales_2'].widget.attrs['readonly'] = True
                if outlet_mater.cnf_cfa_brand_names != '':
                    form1.fields["is_a_cnf_cfa"].initial = outlet_mater.cnf_cfa_brand_names
                else:
                    form1.fields["is_a_cnf_cfa_radio"].initial = 'no'
                    form1.fields['is_a_cnf_cfa'].widget.attrs['readonly'] = True
                if outlet_mater.sp_brand_names != '':
                    form1.fields["is_a_sp"].initial = outlet_mater.cnf_cfa_brand_names
                else:
                    form1.fields["is_a_sp_radio"].initial = 'no'
                    form1.fields['is_a_sp'].widget.attrs['readonly'] = True
                if outlet_mater.transporter_brand_names != '':
                    form1.fields["is_a_transporter"].initial = outlet_mater.cnf_cfa_brand_names
                else:
                    form1.fields["is_a_transporter_radio"].initial = 'no'
                    form1.fields['is_a_transporter'].widget.attrs['readonly'] = True
                if outlet_mater.indenting_agents_brand_names != '':
                    form1.fields["is_a_indenting_agent"].initial = outlet_mater.cnf_cfa_brand_names
                else:
                    form1.fields["is_a_indenting_agent_radio"].initial = 'no'
                    form1.fields['is_a_indenting_agent'].widget.attrs['readonly'] = True
                if outlet_mater.reputation_of_counter:
                    form1.fields["reputation_of_counter"].initial = outlet_mater.reputation_of_counter
        except:
            log.error("No Data exist in Outlet Master for dap_id: " +
                      str(counter_object.dap_id))
        if form.cleaned_data['is_a_cement_counter'] == 'yes' and form.cleaned_data['average_cement_stock']:
            OutletMaster.objects.create(
                no_of_employees=float(form.cleaned_data['no_of_employees']),
                no_of_delivery_vans=float(
                    form.cleaned_data['no_of_delivery_vans']),
                average_cement_stock=form.cleaned_data['average_cement_stock'],
                next_generation_in_business=form.cleaned_data['next_generation_in_business'],
                does_dealer_walk_the_market=form.cleaned_data['does_dealer_walk_the_market'],
                personal_investment_in_business=float(
                    form.cleaned_data['personal_investment_in_business']),
                no_of_sub_dealers=float(
                    form.cleaned_data['no_of_sub_dealers']),
                dap=counter_object,
                updated_by=user_object,
                created_by=user_object)
        else:
            OutletMaster.objects.create(
                no_of_employees=float(form.cleaned_data['no_of_employees']),
                no_of_delivery_vans=float(
                    form.cleaned_data['no_of_delivery_vans']),
                next_generation_in_business=form.cleaned_data['next_generation_in_business'],
                does_dealer_walk_the_market=form.cleaned_data['does_dealer_walk_the_market'],
                personal_investment_in_business=float(
                    form.cleaned_data['personal_investment_in_business']),
                no_of_sub_dealers=float(
                    form.cleaned_data['no_of_sub_dealers']),
                dap=counter_object,
                updated_by=user_object,
                created_by=user_object)
        log.info("Data created in OutletMaster for dap_id: " +
                 str(counter_object.dap_id) + ".")
        form1.fields["so_code"].initial = form.cleaned_data['so_code']
        form1.fields["dap_id"].initial = form.cleaned_data['dap_id']
        if form.cleaned_data['is_a_cement_counter'] == 'no':
            form1.fields['is_a_cement_counter'].initial = 'no'
            form1.fields['is_a_cnf_cfa'].widget.attrs['disabled'] = True
            form1.fields['is_a_cnf_cfa_radio'].widget.attrs['disabled'] = True
            form1.fields['is_a_sp'].widget.attrs['disabled'] = True
            form1.fields['is_a_sp_radio'].widget.attrs['disabled'] = True
            form1.fields['is_a_transporter'].widget.attrs['disabled'] = True
            form1.fields['is_a_transporter_radio'].widget.attrs['disabled'] = True
            form1.fields['is_a_indenting_agent'].widget.attrs['disabled'] = True
            form1.fields['is_a_indenting_agent_radio'].widget.attrs['disabled'] = True
            form1.fields['reputation_of_counter'].widget.attrs['disabled'] = True
        else:
            form1.fields['is_a_cement_counter'].initial = 'yes'
        brandList = BrandsList.objects.all().order_by('brand_name')
        brand_name_list = [('', '-- Select Brand --')]
        for x in brandList:
            option = (x.brand_name, x.brand_name)
            brand_name_list.append(option)
        log.info("Successfully returned to page-2")
        return render(request, 'app1page2.html', {'form': form1, 'brand_name_list': brand_name_list})
    log.error("Form Validation Failed for save_page_1 of views.")
    return redirect('/page-1/' + str(form['so_code'].value()) + '/')


@csrf_exempt
def save_page_2(request):
    log.info("save_page_2 of views, Request " + str(request))
    form = Type1Page2Form(request.POST or None)
    if form.is_valid():
        current_user = UserMaster.objects.get(
            emp_id=form.cleaned_data['so_code'])
        outlet_master_object = OutletMaster.objects.filter(
            dap_id=form.cleaned_data['dap_id']).latest('created_on')
        if outlet_master_object:
            if form.cleaned_data['any_non_cement_business'] == 'yes':
                outlet_master_object.business_1 = form.cleaned_data['business_1']
                if form.cleaned_data['average_monthly_sales_1']:
                    outlet_master_object.average_monthly_sales_1 = float(form.cleaned_data[
                        'average_monthly_sales_1'])
                outlet_master_object.business_2 = form.cleaned_data['business_2']
                if form.cleaned_data['average_monthly_sales_2']:
                    outlet_master_object.average_monthly_sales_2 = float(form.cleaned_data[
                        'average_monthly_sales_2'])
                if form.cleaned_data['non_cement_turnover']:
                    outlet_master_object.non_cement_turnover = float(
                        form.cleaned_data['non_cement_turnover'])
            if form.cleaned_data['is_a_cement_counter'] == 'yes':
                if form.cleaned_data['is_a_cnf_cfa_radio'] == 'yes':
                    outlet_master_object.cnf_cfa_brand_names = form.cleaned_data['is_a_cnf_cfa']
                else:
                    outlet_master_object.cnf_cfa_brand_names = ''
                if form.cleaned_data['is_a_sp_radio'] == 'yes':
                    outlet_master_object.sp_brand_names = form.cleaned_data['is_a_sp']
                else:
                    outlet_master_object.sp_brand_names = ''
                if form.cleaned_data['is_a_transporter_radio'] == 'yes':
                    outlet_master_object.transporter_brand_names = form.cleaned_data[
                        'is_a_transporter']
                else:
                    outlet_master_object.transporter_brand_names = ''
                if form.cleaned_data['is_a_indenting_agent_radio'] == 'yes':
                    outlet_master_object.indenting_agents_brand_names = form.cleaned_data[
                        'is_a_indenting_agent']
                else:
                    outlet_master_object.indenting_agents_brand_names = ''
                outlet_master_object.reputation_of_counter = form.cleaned_data[
                    'reputation_of_counter']

            if form.cleaned_data['any_non_cement_business'] == 'yes' or form.cleaned_data['is_a_cement_counter'] == 'yes':
                outlet_master_object.updated_by = current_user
                outlet_master_object.save()
                log.info("Data successfully updated in OutletMaster for dap_id: " +
                         str(form.cleaned_data['dap_id']) + ".")
            else:
                log.info(
                    'Nothing to save as any_non_cement_business is "No" and is_a_cement_counter is "No".')
            if form.cleaned_data['is_a_cement_counter'] == 'no':
                log.info("Successfully returned to submit page.")
                return redirect('/submit/')
        else:
            log.error("No counder exist for dap_id: " +
                      str(form.cleaned_data['dap_id']) + ".")
            return redirect('/home/')
        form1 = Type1Page3Form()
        form1.fields['dap_id'].initial = form.cleaned_data['dap_id']
        form1.fields['so_code'].initial = form.cleaned_data['so_code']
        try:
            # import pdb; pdb.set_trace();
            brand_master_object = BrandMaster.objects.filter(
                dap_id=form.cleaned_data['dap_id']).latest('created_on')
            brand_master_object1 = BrandMaster.objects.filter(
                dap_id=form.cleaned_data['dap_id'], created_on=brand_master_object.created_on)
            for i, brand in enumerate(brand_master_object1):
                form1.fields['total_potential'].initial = brand.total_potential
                form1.fields['wholesale_volume'].initial = brand.total_wholesale_volume
                form1.fields['retail_volume'].initial = float(
                    brand.total_potential) - float(brand.total_wholesale_volume)
                form1.fields['brand_' + str(i + 1)].initial = brand.brand_id
                form1.fields['wholesale_brand_' +
                             str(i + 1)].initial = brand.wholesale_volume
                form1.fields['opc_brand_' + str(i + 1)].initial = brand.opc
                form1.fields['ppc_brand_' + str(i + 1)].initial = brand.ppc
                form1.fields['pcc_brand_' + str(i + 1)].initial = brand.pcc
                form1.fields['psc_brand_' + str(i + 1)].initial = brand.psc
        except:
            log.error("No data fetched from BrandMaster for dap_id: " +
                      str(form.cleaned_data['dap_id']))
        brandList = BrandsList.objects.all().order_by('brand_name')
        brand_name_list = [('', '-- Select Brand --')]
        for x in brandList:
            option = (x.brand_id, x.brand_name)
            brand_name_list.append(option)
        log.info("Successfully returned to page-3.")
        return render(request, 'app1page3.html', {'form': form1, 'brand_name_list': brand_name_list})
    log.error("Form Validation Failed for save_page_2 of views.")
    return render(request, 'app1page2.html', {'form': form})


@csrf_exempt
def save_page_3(request):
    log.info("save_page_3 of views, Request " + str(request))
    try:
        # import pdb; pdb.set_trace();
        form = Type1Page3Form(request.POST or None)
        form1 = Type1Page4PreviewForm()
        if form.is_valid():
            current_user = UserMaster.objects.get(
                emp_id=form.cleaned_data['so_code'])
            current_timestamp = timezone.now()
            dap_id = form.cleaned_data['dap_id']
            total_potential = form.cleaned_data['total_potential']
            wholesale_volume = form.cleaned_data['wholesale_volume']
            retail_volume = form.cleaned_data['retail_volume']
            brand_1 = form.cleaned_data['brand_1']
            wholesale_brand_1 = form.cleaned_data['wholesale_brand_1']
            opc_brand_1 = form.cleaned_data['opc_brand_1']
            ppc_brand_1 = form.cleaned_data['ppc_brand_1']
            pcc_brand_1 = form.cleaned_data['pcc_brand_1']
            psc_brand_1 = form.cleaned_data['psc_brand_1']
            brand_2 = form.cleaned_data['brand_2']
            wholesale_brand_2 = form.cleaned_data['wholesale_brand_2']
            opc_brand_2 = form.cleaned_data['opc_brand_2']
            ppc_brand_2 = form.cleaned_data['ppc_brand_2']
            pcc_brand_2 = form.cleaned_data['pcc_brand_2']
            psc_brand_2 = form.cleaned_data['psc_brand_2']
            brand_3 = form.cleaned_data['brand_3']
            wholesale_brand_3 = form.cleaned_data['wholesale_brand_3']
            opc_brand_3 = form.cleaned_data['opc_brand_3']
            ppc_brand_3 = form.cleaned_data['ppc_brand_3']
            pcc_brand_3 = form.cleaned_data['pcc_brand_3']
            psc_brand_3 = form.cleaned_data['psc_brand_3']
            brand_4 = form.cleaned_data['brand_4']
            wholesale_brand_4 = form.cleaned_data['wholesale_brand_4']
            opc_brand_4 = form.cleaned_data['opc_brand_4']
            ppc_brand_4 = form.cleaned_data['ppc_brand_4']
            pcc_brand_4 = form.cleaned_data['pcc_brand_4']
            psc_brand_4 = form.cleaned_data['psc_brand_4']
            brand_5 = form.cleaned_data['brand_5']
            wholesale_brand_5 = form.cleaned_data['wholesale_brand_5']
            opc_brand_5 = form.cleaned_data['opc_brand_5']
            ppc_brand_5 = form.cleaned_data['ppc_brand_5']
            pcc_brand_5 = form.cleaned_data['pcc_brand_5']
            psc_brand_5 = form.cleaned_data['psc_brand_5']
            other_brand_retailsale = form.cleaned_data['other_brand_retailsale']
            other_brand_wholesale = form.cleaned_data['other_brand_wholesale']
            comment = form.cleaned_data['comment']

            outlet_details_object = OutletDetails.objects.get(dap_id=dap_id)
            if comment:
                outlet_details_object.comment = comment
                outlet_details_object.save()
                log.info(
                    "Comment successfully saved in OutletDetails for dap_id: " + str(dap_id))
            if total_potential != 0 and wholesale_volume != 0 and retail_volume != 0:
                if brand_1 != '' and (wholesale_brand_1 != '0' or opc_brand_1 != '0' or ppc_brand_1 != '0' or pcc_brand_1 != '0' or psc_brand_1 != '0'):
                    brand_list_object = BrandsList.objects.get(
                        brand_id=brand_1)
                    BrandMaster.objects.create(
                        dap=outlet_details_object,
                        brand=brand_list_object,
                        brand_name=brand_list_object.brand_name,
                        total_potential=total_potential,
                        total_wholesale_volume=wholesale_volume,
                        total_retail_volume=retail_volume,
                        wholesale_volume=wholesale_brand_1,
                        retail_volume=(float(opc_brand_1) if opc_brand_1 else 0) + (float(ppc_brand_1) if ppc_brand_1 else 0) +
                        (float(pcc_brand_1) if pcc_brand_1 else 0) +
                        (float(psc_brand_1) if psc_brand_1 else 0),
                        opc=opc_brand_1,
                        ppc=ppc_brand_1,
                        pcc=pcc_brand_1,
                        psc=psc_brand_1,
                        created_on=current_timestamp,
                        created_by=current_user,
                        updated_by=current_user,
                    )
                    form1.fields['brand_1'].initial = brand_list_object.brand_name
                    form1.fields['wholesale_brand_1'].initial = wholesale_brand_1
                    form1.fields['opc_brand_1'].initial = opc_brand_1
                    form1.fields['ppc_brand_1'].initial = ppc_brand_1
                    form1.fields['pcc_brand_1'].initial = pcc_brand_1
                    form1.fields['psc_brand_1'].initial = psc_brand_1
                    log.info(
                        "Record successfully saved in BrandMaster for brand_1 against dap_id: " + str(dap_id))
                if brand_2 != '' and (wholesale_brand_2 != '0' or opc_brand_2 != '0' or ppc_brand_2 != '0' or pcc_brand_2 != '0' or psc_brand_2 != '0'):
                    brand_list_object = BrandsList.objects.get(
                        brand_id=brand_2)
                    BrandMaster.objects.create(
                        dap=outlet_details_object,
                        brand=brand_list_object,
                        brand_name=brand_list_object.brand_name,
                        total_potential=total_potential,
                        total_wholesale_volume=wholesale_volume,
                        total_retail_volume=retail_volume,
                        wholesale_volume=wholesale_brand_2,
                        retail_volume=(float(opc_brand_2) if opc_brand_2 else 0) + (float(ppc_brand_2) if ppc_brand_2 else 0) +
                        (float(pcc_brand_2) if pcc_brand_2 else 0) +
                        (float(psc_brand_2) if psc_brand_2 else 0),
                        opc=opc_brand_2,
                        ppc=ppc_brand_2,
                        pcc=pcc_brand_2,
                        psc=psc_brand_2,
                        created_on=current_timestamp,
                        created_by=current_user,
                        updated_by=current_user,
                    )
                    form1.fields['brand_2'].initial = brand_list_object.brand_name
                    form1.fields['wholesale_brand_2'].initial = wholesale_brand_2
                    form1.fields['opc_brand_2'].initial = opc_brand_2
                    form1.fields['ppc_brand_2'].initial = ppc_brand_2
                    form1.fields['pcc_brand_2'].initial = pcc_brand_2
                    form1.fields['psc_brand_2'].initial = psc_brand_2
                    log.info(
                        "Record successfully saved in BrandMaster for brand_2 against dap_id: " + str(dap_id))
                if brand_3 != '' and (wholesale_brand_3 != '0' or opc_brand_3 != '0' or ppc_brand_3 != '0' or pcc_brand_3 != '0' or psc_brand_3 != '0'):
                    brand_list_object = BrandsList.objects.get(
                        brand_id=brand_3)
                    BrandMaster.objects.create(
                        dap=outlet_details_object,
                        brand=brand_list_object,
                        brand_name=brand_list_object.brand_name,
                        total_potential=total_potential,
                        total_wholesale_volume=wholesale_volume,
                        total_retail_volume=retail_volume,
                        wholesale_volume=wholesale_brand_3,
                        retail_volume=(float(opc_brand_3) if opc_brand_3 else 0) + (float(ppc_brand_3) if ppc_brand_3 else 0) +
                        (float(pcc_brand_3) if pcc_brand_3 else 0) +
                        (float(psc_brand_3) if psc_brand_3 else 0),
                        opc=opc_brand_3,
                        ppc=ppc_brand_3,
                        pcc=pcc_brand_3,
                        psc=psc_brand_3,
                        created_on=current_timestamp,
                        created_by=current_user,
                        updated_by=current_user,
                    )
                    form1.fields['brand_3'].initial = brand_list_object.brand_name
                    form1.fields['wholesale_brand_3'].initial = wholesale_brand_3
                    form1.fields['opc_brand_3'].initial = opc_brand_3
                    form1.fields['ppc_brand_3'].initial = ppc_brand_3
                    form1.fields['pcc_brand_3'].initial = pcc_brand_3
                    form1.fields['psc_brand_3'].initial = psc_brand_3
                    log.info(
                        "Record successfully saved in BrandMaster for brand_3 against dap_id: " + str(dap_id))
                if brand_4 != '' and (wholesale_brand_4 != '0' or opc_brand_4 != '0' or ppc_brand_4 != '0' or pcc_brand_4 != '0' or psc_brand_4 != '0'):
                    brand_list_object = BrandsList.objects.get(
                        brand_id=brand_4)
                    BrandMaster.objects.create(
                        dap=outlet_details_object,
                        brand=brand_list_object,
                        brand_name=brand_list_object.brand_name,
                        total_potential=total_potential,
                        total_wholesale_volume=wholesale_volume,
                        total_retail_volume=retail_volume,
                        wholesale_volume=wholesale_brand_4,
                        retail_volume=(float(opc_brand_4) if opc_brand_4 else 0) + (float(ppc_brand_4) if ppc_brand_4 else 0) +
                        (float(pcc_brand_4) if pcc_brand_4 else 0) +
                        (float(psc_brand_4) if psc_brand_4 else 0),
                        opc=opc_brand_4,
                        ppc=ppc_brand_4,
                        pcc=pcc_brand_4,
                        psc=psc_brand_4,
                        created_on=current_timestamp,
                        created_by=current_user,
                        updated_by=current_user,
                    )
                    form1.fields['brand_4'].initial = brand_list_object.brand_name
                    form1.fields['wholesale_brand_4'].initial = wholesale_brand_4
                    form1.fields['opc_brand_4'].initial = opc_brand_4
                    form1.fields['ppc_brand_4'].initial = ppc_brand_4
                    form1.fields['pcc_brand_4'].initial = pcc_brand_4
                    form1.fields['psc_brand_4'].initial = psc_brand_4
                    log.info(
                        "Record successfully saved in BrandMaster for brand_4 against dap_id: " + str(dap_id))
                if brand_5 != '' and (wholesale_brand_5 != '0' or opc_brand_5 != '0' or ppc_brand_5 != '0' or pcc_brand_5 != '0' or psc_brand_5 != '0'):
                    brand_list_object = BrandsList.objects.get(
                        brand_id=brand_5)
                    BrandMaster.objects.create(
                        dap=outlet_details_object,
                        brand=brand_list_object,
                        brand_name=brand_list_object.brand_name,
                        total_potential=total_potential,
                        total_wholesale_volume=wholesale_volume,
                        total_retail_volume=retail_volume,
                        wholesale_volume=wholesale_brand_5,
                        retail_volume=(float(opc_brand_5) if opc_brand_5 else 0) + (float(ppc_brand_5) if ppc_brand_5 else 0) +
                        (float(pcc_brand_5) if pcc_brand_5 else 0) +
                        (float(psc_brand_5) if psc_brand_5 else 0),
                        opc=opc_brand_5,
                        ppc=ppc_brand_5,
                        pcc=pcc_brand_5,
                        psc=psc_brand_5,
                        created_on=current_timestamp,
                        created_by=current_user,
                        updated_by=current_user,
                    )
                    form1.fields['brand_5'].initial = brand_list_object.brand_name
                    form1.fields['wholesale_brand_5'].initial = wholesale_brand_5
                    form1.fields['opc_brand_5'].initial = opc_brand_5
                    form1.fields['ppc_brand_5'].initial = ppc_brand_5
                    form1.fields['pcc_brand_5'].initial = pcc_brand_5
                    form1.fields['psc_brand_5'].initial = psc_brand_5
                    log.info(
                        "Record successfully saved in BrandMaster for brand_5 against dap_id: " + str(dap_id))
                # form1.fields['dap_id'].initial = dap_id
                form1.fields['total_potential'].initial = total_potential
                form1.fields['wholesale_volume'].initial = wholesale_volume
                form1.fields['retail_volume'].initial = retail_volume
            outlet_data = fetch_outlet_data_by_dap_id(dap_id)
            print(outlet_data)
            form1.fields['so_code'].initial = outlet_data[0]['so_code']
            form1.fields['dap_id'].initial = outlet_data[0]['dap_id']
            form1.fields['counter_name'].initial = outlet_data[0]['counter_name']
            form1.fields['state'].initial = outlet_data[0]['state']
            form1.fields['district'].initial = outlet_data[0]['district']
            form1.fields['city'].initial = outlet_data[0]['city']
            form1.fields['phone_number'].initial = outlet_data[0]['phone_number']
            form1.fields['geo_location'].initial = outlet_data[0]['geo_location']
            form1.fields['block_taluka_mandal'].initial = outlet_data[0]['block_taluka_mandal']
            form1.fields['is_a_cement_counter'].initial = outlet_data[0]['is_cement_counter']
            form1.fields['dob'].initial = outlet_data[0]['dob']
            form1.fields['comment'].initial = outlet_data[0]['comment']
            form1.fields['no_of_employees'].initial = outlet_data[0]['outlet_master'][0]['no_of_employees']
            form1.fields['no_of_delivery_vans'].initial = outlet_data[0]['outlet_master'][0]['no_of_delivery_vans']
            form1.fields['average_cement_stock'].initial = outlet_data[0]['outlet_master'][0]['average_cement_stock']
            form1.fields['next_generation_in_business'].initial = outlet_data[0]['outlet_master'][0]['next_generation_in_business']
            form1.fields['does_dealer_walk_the_market'].initial = outlet_data[0]['outlet_master'][0]['does_dealer_walk_the_market']
            form1.fields['personal_investment_in_business'].initial = outlet_data[0]['outlet_master'][0]['personal_investment_in_business']
            form1.fields['no_of_sub_dealers'].initial = outlet_data[0]['outlet_master'][0]['no_of_sub_dealers']
            form1.fields['non_cement_turnover'].initial = outlet_data[0]['outlet_master'][0]['non_cement_turnover']
            form1.fields['business_1'].initial = outlet_data[0]['outlet_master'][0]['business_1']
            form1.fields['average_monthly_sales_1'].initial = outlet_data[0]['outlet_master'][0]['average_monthly_sales_1']
            form1.fields['business_2'].initial = outlet_data[0]['outlet_master'][0]['business_2']
            form1.fields['average_monthly_sales_2'].initial = outlet_data[0]['outlet_master'][0]['average_monthly_sales_2']
            form1.fields['is_a_cnf_cfa'].initial = outlet_data[0]['outlet_master'][0]['cnf_cfa_brand_names']
            form1.fields['is_a_sp'].initial = outlet_data[0]['outlet_master'][0]['sp_brand_names']
            form1.fields['is_a_transporter'].initial = outlet_data[0]['outlet_master'][0]['transporter_brand_names']
            form1.fields['is_a_indenting_agent'].initial = outlet_data[0]['outlet_master'][0]['indenting_agents_brand_names']
            form1.fields['reputation_of_counter'].initial = outlet_data[0]['outlet_master'][0]['reputation_of_counter']
            log.info("Successfully returned to preview page.")
            return render(request, 'preview.html', {'form': form1})
        log.error("Form Validation Failed of views.")
        return render(request, 'errors.html', {'form': form})
    except Exception as ex:
        log.error("Exception occured while save_page_3, Error msg: " + str(ex))
        return redirect('/home/')


@csrf_exempt
def submit(request):
    log.info("submit of views, Request " + str(request))
    return render(request, 'end.html')


@csrf_exempt
def generate_report(request):
    log.info("generate_report of views, Request " + str(request))
    try:
        file_name = "Report.xlsx"
        t1 = threading.Thread(target=save_report, args=(file_name,))
        t1.start()
        log.info("Workbook saving starts with name " + str(file_name) + ".")
        return JsonResponse(prepare_response(message="Workbook saving starts with name " + str(file_name) + ".", status="SUCCESS", success=True, info="Workbook saving starts successfully."))
    except Exception as ex:
        log.error("Exception occured while start threading, Error msg: ", str(ex))
        return JsonResponse(prepare_response(message="Exception occured while generating report, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again."))


def save_report(file_name):
    log.info("save_report of views, Filename " + str(file_name))
    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': 1})
    date_format = workbook.add_format({'num_format': 'dd/mm/yyyy hh:mm:ss'})
    change_color = workbook.add_format({
        'bg_color': '#33FF5B',
        'color': 'black',
        'valign': 'top',
        'border': 1,
        'bold': 1
    })
    change_color1 = workbook.add_format({
        'bg_color': '#33CEFF',
        'color': 'black',
        'valign': 'top',
        'border': 1,
        'bold': 1
    })
    row = 0
    column = 63
    brand_heading = ['KONARK', 'DALMIA', 'DSP', 'AMBUJA', 'AMBUJA +', 'ACC', 'ACC F2R', 'NUVOCO', 'NUVOCO - CONCRETO', 'ULTRATECH', 'ULTRATECH PREMIUM', 'BIRLA SAMRAT', 'BIRLA UNIQUE & ULTIMATE', 'BIRLA GOLD', 'BIRLA GOLD CLASSIC', 'SHREE & BANGUR', 'PRISM', 'EMAMI', 'JSW', 'JK LAKSHMI', 'JK PLATINUM ', 'MAHA', 'MAHA SOLID +', 'JAYPEE', 'RAMCO', 'ICL', 'Chettinad', 'Bharthi', 'JK Cement', 'Penna', 'Zuari', 'Rain / Priya', 'Topcem',
                     'STAR', 'Amrit', 'JUD', 'Max', 'Imports', 'OTHERS', 'K D Cement (Surksha)', 'Hill Cement (Taj)', 'Black Tiger', 'Surya Gold', 'INDUSTRY', 'AMBUJA Compocem', 'CENTURY CLASSIC', 'ARASU', 'TNPL', 'Other -B Brand', 'Birla A1', 'Vasavdatta', 'KCP', 'Sagar', 'BIRLA SAKTHI', 'DECCAN', 'PARASAKTHI', 'BAVIYA', 'PANYAM', 'BIRLA A1', 'TEXMO', 'NAGAARJUNA', 'LANCO GOLD', 'BEEMA SAKTHI', 'MARUTHI', 'ULTRA GOLD', 'VIJAY GOLD', 'INDUSTRY']
    for counter, item in enumerate(brand_heading):
        worksheet.write(row, column + 3, item, bold)
        worksheet.write(row + 1, column, "W/S",
                        change_color if counter % 2 == 0 else change_color1)
        worksheet.write(row + 1, column + 1, "PSC",
                        change_color if counter % 2 == 0 else change_color1)
        worksheet.write(row + 1, column + 2, "PCC",
                        change_color if counter % 2 == 0 else change_color1)
        worksheet.write(row + 1, column + 3, "PPC",
                        change_color if counter % 2 == 0 else change_color1)
        worksheet.write(row + 1, column + 4, "OPC",
                        change_color if counter % 2 == 0 else change_color1)
        worksheet.write(row + 1, column + 5, item + " TOTAL",
                        change_color if counter % 2 == 0 else change_color1)
        column += 6
    brandIndex = {'B1': 0,   'B10': 9,   'B11': 10,   'B12': 11,   'B13': 12,   'B14': 13,   'B15': 14,   'B16': 15,   'B17': 16,   'B18': 17,   'B19': 18,   'B2': 1,   'B20': 19,   'B21': 20,   'B22': 21,   'B23': 22,   'B24': 23,   'B25': 24,   'B26': 25,   'B27': 26,   'B28': 27,   'B29': 28,
                  'B3': 2,   'B30': 29,   'B31': 30,   'B32': 31,   'B33': 32,   'B34': 33,   'B35': 34,   'B36': 35,   'B37': 36,   'B38': 37,   'B39': 38,   'B4': 3,   'B40': 39,   'B41': 40,   'B42': 41,   'B43': 42,   'B45': 44,   'B46': 45,   'B5': 4,   'B6': 5,   'B7': 6,   'B8': 7,   'B9': 8}
    row = 1
    column = 0
    excel_heading = ['ID', 'DAP Code', 'STATE', 'REGION', 'COUNTER CODE', 'COUNTER NAME', 'DEALER CODE', 'DEALER NAME', 'COUNTER TYPE', 'UNIT', 'DISTRICT', 'BLOCK/Taluka', 'SALES OFFICER', 'ASM', 'RSM', 'Coach', 'ZSM/State Head', 'Cluster Head', 'Regional Sales Head', 'Potential Slab', 'Total Potential (W/S +Retail) - Original', 'Wholesale Potential - Original', 'Retail Potential - Original', 'Total Potential (W/S +Retail) - New', 'Wholesale Potential - New', 'Retail Potential - New', 'SO EMP ID', 'ASM EMP ID', 'RSM EMP ID', 'COACH EMP ID', 'SH EMP ID', 'Cluster head EMP ID', 'HEAD SALES', 'Others', 'PH NUMBER', 'Is this a cement counter', 'LATTITUDE', 'LONGITUDE', 'No of Employees',
                     'No of Delivery Vans', 'Average  Cement stock (MT/Month)', 'Next generation in business (Y/N)', 'Does the dealer /Subdealer go to market (Y/N)', 'Personal Investment in business (Inr Laks)', 'No of subdealer', 'Any NON -CEMENT Business (Y/N)', 'Total avg.monthly sales from other businesses (inr laks)', 'Business 1 *', 'Avg.Monthly sales1 (Inr laks)', 'Business 2 *', 'Avg.Monthly sales2 (Inr laks)', 'is a CNF/CFA ? (Y/N)', 'If Yes Brand *', 'is a SP ? (Y/N)', 'If Yes Brand *', 'is a Transporter ? (Y/N)', 'If Yes Brand *', 'is an indenting agent ? (Y/N)', 'If Yes Brand *', 'Repuattion of the counter (Good / Average/Poor)', 'SAP Created On', 'SAP Updated On', 'Is Updated']
    for item in excel_heading:
        worksheet.write(row, column, item, bold)
        column += 1
    # worksheet.autofilter('A1:QU1')
    try:
        qs = OutletDetails.objects.all()
        finalResponseList = []
        if qs:
            for counterObj in qs:
                log.info("Writing dap_id: " + str(counterObj.dap_id))
                row = row + 1
                worksheet.write(row, 0, str(counterObj.dap_id))
                worksheet.write(row, 1, str(counterObj.dap_id))
                worksheet.write(row, 2, str(counterObj.state)
                                if counterObj.state else "")
                worksheet.write(row, 3, str(counterObj.region)
                                if counterObj.region else "")
                worksheet.write(row, 4, counterObj.sap_code)
                worksheet.write(row, 5, counterObj.counter_name)
                worksheet.write(row, 8, counterObj.counter_type)
                worksheet.write(row, 9, counterObj.unit)
                worksheet.write(row, 10, str(counterObj.district)
                                if counterObj.district else "")
                worksheet.write(row, 11, str(
                    counterObj.block_taluka_mandal) if counterObj.block_taluka_mandal else "")
                worksheet.write(row, 12, str(counterObj.so_code)
                                if counterObj.so_code else "")
                worksheet.write(row, 13, str(counterObj.asm)
                                if counterObj.asm else "")
                worksheet.write(row, 14, str(counterObj.rsm)
                                if counterObj.rsm else "")
                worksheet.write(row, 15, str(counterObj.coach)
                                if counterObj.coach else "")
                worksheet.write(row, 16, str(counterObj.zsm_state_head)
                                if counterObj.zsm_state_head else "")
                worksheet.write(row, 17, str(counterObj.cluster_head)
                                if counterObj.cluster_head else "")
                worksheet.write(row, 20, counterObj.total_potential)
                worksheet.write(row, 21, counterObj.wholesale_volume)
                worksheet.write(row, 22, counterObj.retail_volume)
                worksheet.write(row, 26, str(
                    counterObj.so_code.emp_id) if counterObj.so_code else "")
                worksheet.write(row, 27, str(
                    counterObj.asm.emp_id) if counterObj.asm else "")
                worksheet.write(row, 28, str(
                    counterObj.rsm.emp_id) if counterObj.rsm else "")
                worksheet.write(row, 29, str(
                    counterObj.coach.emp_id) if counterObj.coach else "")
                worksheet.write(row, 30, str(
                    counterObj.zsm_state_head.emp_id) if counterObj.zsm_state_head else "")
                worksheet.write(row, 31, str(
                    counterObj.cluster_head.emp_id) if counterObj.cluster_head else "")
                worksheet.write(row, 34, str(counterObj.phone_number)
                                if counterObj.phone_number else "")
                worksheet.write(row, 35, counterObj.is_cement_counter)
                worksheet.write(row, 36, str(counterObj.geo_location.split(",")[0]).strip(
                ) if counterObj.geo_location and len(counterObj.geo_location.split(",")) > 0 else "")
                worksheet.write(row, 37, str(counterObj.geo_location.split(",")[1]).strip(
                ) if counterObj.geo_location and len(counterObj.geo_location.split(",")) > 1 else "")
                dap_updated_on = counterObj.updated_on
                for dmaster in counterObj.outletmaster_set.all().order_by('-created_on')[:1]:
                    dap_updated_on = dmaster.created_on
                    worksheet.write(row, 38, dmaster.no_of_employees)
                    worksheet.write(row, 39, dmaster.no_of_delivery_vans)
                    worksheet.write(row, 40, dmaster.average_cement_stock)
                    worksheet.write(
                        row, 41, dmaster.next_generation_in_business)
                    worksheet.write(
                        row, 42, dmaster.does_dealer_walk_the_market)
                    worksheet.write(
                        row, 43, dmaster.personal_investment_in_business)
                    worksheet.write(row, 44, dmaster.no_of_sub_dealers)
                    worksheet.write(row, 46, dmaster.non_cement_turnover)
                    worksheet.write(row, 47, dmaster.business_1)
                    worksheet.write(row, 48, dmaster.average_monthly_sales_1)
                    worksheet.write(row, 49, dmaster.business_2)
                    worksheet.write(row, 50, dmaster.average_monthly_sales_2)
                    if dmaster.cnf_cfa_brand_names:
                        worksheet.write(row, 51, 'Y')
                        worksheet.write(row, 52, dmaster.cnf_cfa_brand_names)
                    else:
                        worksheet.write(row, 51, 'N')
                    if dmaster.sp_brand_names:
                        worksheet.write(row, 53, 'Y')
                        worksheet.write(row, 54, dmaster.sp_brand_names)
                    else:
                        worksheet.write(row, 53, 'N')
                    if dmaster.transporter_brand_names:
                        worksheet.write(row, 55, 'Y')
                        worksheet.write(
                            row, 56, dmaster.transporter_brand_names)
                    else:
                        worksheet.write(row, 55, 'N')
                    if dmaster.indenting_agents_brand_names:
                        worksheet.write(row, 57, 'Y')
                        worksheet.write(
                            row, 58, dmaster.indenting_agents_brand_names)
                    else:
                        worksheet.write(row, 57, 'N')
                    worksheet.write(row, 59, dmaster.reputation_of_counter)

                try:
                    latest_created_on = counterObj.brandmaster_set.all().order_by(
                        '-created_on')[:1][0].created_on
                    dap_updated_on = latest_created_on
                    for bmaster in counterObj.brandmaster_set.all().filter(created_on=latest_created_on):
                        worksheet.write(row, 23, bmaster.total_potential)
                        worksheet.write(
                            row, 24, bmaster.total_wholesale_volume)
                        worksheet.write(row, 25, bmaster.total_retail_volume)
                        colIndex = brandIndex[bmaster.brand_id] * 6 + 63
                        worksheet.write(
                            row, colIndex, bmaster.total_wholesale_volume)
                        worksheet.write(row, colIndex + 1, bmaster.opc)
                        worksheet.write(row, colIndex + 2, bmaster.ppc)
                        worksheet.write(row, colIndex + 3, bmaster.pcc)
                        worksheet.write(row, colIndex + 4, bmaster.psc)
                        worksheet.write(row, colIndex + 5, ((float(bmaster.total_wholesale_volume) if bmaster.total_wholesale_volume else 0) + (float(bmaster.opc) if bmaster.opc else 0) + (
                            float(bmaster.ppc) if bmaster.ppc else 0) + (float(bmaster.pcc) if bmaster.pcc else 0) + (float(bmaster.psc) if bmaster.psc else 0)))
                except Exception as ex:
                    log.error("No brand data found for dap_id: " +
                              str(counterObj.dap_id))
                worksheet.write(row, 60, counterObj.created_on.strftime(
                    '%d %b %Y %H:%M:%S'))
                worksheet.write(row, 61, dap_updated_on.strftime(
                    '%d %b %Y %H:%M:%S'))
                worksheet.write(row, 62, "Yes" if dap_updated_on !=
                                counterObj.created_on else "No")
            workbook.close()
            log.info("Workbook saved of name " + str(file_name) + ".")
            return JsonResponse(prepare_response(message="Workbook saved with name " + str(file_name) + ".", status="SUCCESS", success=True, info="Workbook saved successfully."))
        log.info("No Data exist for the selected filter.")
    except Exception as ex:
        log.error(
            "Exception occured while generating report, Error msg: " + str(ex))
        log.error("Traceback : " + str(traceback.format_exc()))
        return JsonResponse(prepare_response(message="Exception occured while generating report, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again."))


@csrf_exempt
def download_report(request):
    log.info("download_report in views, Request: " + str(request))
    try:
        filename = open(os.path.join(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__)))) + "/Report.xlsx", 'rb')
        return FileResponse(filename, as_attachment=True)
    except Exception as ex:
        log.error(
            "Exception occured while downloading Excel file, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while downloading Excel file, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again."))


@csrf_exempt
def get_outlets_data_by_filter(request):
    log.info("get_outlets_data_by_filter in views, Request: " + str(request))
    try:
        if not request.headers['Authorization'] == 'TokenhgjhgJHGJhjgbht7657@#&yd@p':
            return JsonResponse(prepare_response(message="Authentication Failed.", status="FAILURE", success=False, info="Authentication Failed.", responseCode=401))
        if request.method == 'POST':
            received_json_data = json.loads(request.body.decode('utf-8'))
            log.info(
                "Payload received for get_outlets_data_by_filter is " + str(received_json_data))
            if 'state_id' in received_json_data:
                try:
                    stateMaster = StateMaster.objects.get(
                        state_id=int(received_json_data['state_id']))
                    if stateMaster:
                        qs = OutletDetails.objects.filter(state=stateMaster)
                    else:
                        return JsonResponse(prepare_response(message="Unable to process your request as the values are incorrect.", status="FAILURE", success=False, info="Selected state does not exist.", responseCode=5003))
                except Exception as ex:
                    return JsonResponse(prepare_response(message="Unable to process your request as the values are incorrect.", status="FAILURE", success=False, info="Selected state does not exist.", responseCode=5003))
            else:
                return JsonResponse(prepare_response(message="Unable to process your request as the values are incorrect.", status="FAILURE", success=False, info="'state_id' is mandatory field.", responseCode=4001))

            if "only_sap_counters" in received_json_data and received_json_data['only_sap_counters'] and ("only_non_sap_counters" not in received_json_data or ("only_non_sap_counters" in received_json_data and not received_json_data['only_non_sap_counters'])):
                qs = qs.exclude(
                    sap_code__isnull=True).exclude(sap_code__exact='')
            elif "only_non_sap_counters" in received_json_data and received_json_data['only_non_sap_counters'] and ("only_sap_counters" not in received_json_data or ("only_sap_counters" in received_json_data and not received_json_data['only_sap_counters'])):
                qs = qs.filter(
                    sap_code__isnull=True) | qs.filter(sap_code__exact='')
            elif ("only_non_sap_counters" not in received_json_data and "only_sap_counters" not in received_json_data) or ("only_sap_counters" in received_json_data and not received_json_data['only_sap_counters'] and "only_non_sap_counters" in received_json_data and not received_json_data['only_non_sap_counters']):
                qs = qs.all()
            else:
                return JsonResponse(prepare_response(message="Unable to process your request as the values are incorrect.", status="FAILURE", success=False, info="Both 'only_sap_counters' and 'only_non_sap_counters' can not be true, Set both keys value as 'false' for complete data.", responseCode=4001))
            finalResponseList = []
            if qs:
                for counterObj in qs:
                    log.info("Writing dap_id: " + str(counterObj.dap_id))
                    innerDict = {}
                    innerDict['dapId'] = counterObj.dap_id
                    innerDict['isVerified'] = counterObj.is_verified
                    innerDict['verifiedBy'] = str(
                        counterObj.verified_by) if counterObj.verified_by else ""
                    innerDict['isSapCodeVerified'] = counterObj.is_sap_code_verified
                    innerDict['sapCodeVerifiedBy'] = str(
                        counterObj.sap_code_verified_by) if counterObj.sap_code_verified_by else ""
                    innerDict['referenceId'] = str(
                        counterObj.so_code.emp_id) if counterObj.so_code else ""
                    innerDict['salesOfficerName'] = str(
                        counterObj.so_code) if counterObj.so_code else ""
                    innerDict['counterCode'] = counterObj.sap_code
                    innerDict['counterName'] = counterObj.counter_name
                    innerDict['emailId'] = counterObj.email_id
                    innerDict['region'] = str(
                        counterObj.region) if counterObj.region else ""
                    innerDict['state'] = str(
                        counterObj.state) if counterObj.state else ""
                    innerDict['district'] = str(
                        counterObj.district) if counterObj.district else ""
                    innerDict['city'] = str(
                        counterObj.city) if counterObj.city else ""
                    innerDict['blockTalukaMandal'] = str(
                        counterObj.block_taluka_mandal) if counterObj.block_taluka_mandal else ""
                    innerDict['phoneNumber'] = str(
                        counterObj.phone_number) if counterObj.phone_number else ""
                    innerDict['geoLocation'] = counterObj.geo_location
                    innerDict['latitude'] = str(counterObj.geo_location.split(",")[0]).strip(
                    ) if counterObj.geo_location and len(counterObj.geo_location.split(",")) > 0 else ""
                    innerDict['longitude'] = str(counterObj.geo_location.split(",")[1]).strip(
                    ) if counterObj.geo_location and len(counterObj.geo_location.split(",")) > 1 else ""
                    innerDict['isCementCounter'] = counterObj.is_cement_counter
                    innerDict['counterType'] = counterObj.counter_type
                    innerDict['dob'] = counterObj.dob
                    innerDict['comment'] = counterObj.comment
                    innerDict['unit'] = counterObj.unit
                    innerDict['asmId'] = str(
                        counterObj.asm.emp_id) if counterObj.asm else ""
                    innerDict['asmName'] = str(
                        counterObj.asm) if counterObj.asm else ""
                    innerDict['rsmId'] = str(
                        counterObj.rsm.emp_id) if counterObj.rsm else ""
                    innerDict['rsmName'] = str(
                        counterObj.rsm) if counterObj.rsm else ""
                    innerDict['coachId'] = str(
                        counterObj.coach.emp_id) if counterObj.coach else ""
                    innerDict['coachName'] = str(
                        counterObj.coach) if counterObj.coach else ""
                    innerDict['stateHeadId'] = str(
                        counterObj.zsm_state_head.emp_id) if counterObj.zsm_state_head else ""
                    innerDict['stateHeadName'] = str(
                        counterObj.zsm_state_head) if counterObj.zsm_state_head else ""
                    innerDict['clusterHeadId'] = str(
                        counterObj.cluster_head.emp_id) if counterObj.cluster_head else ""
                    innerDict['clusterHeadName'] = str(
                        counterObj.cluster_head) if counterObj.cluster_head else ""
                    innerDict['totalPotential'] = counterObj.total_potential
                    innerDict['wholesaleVolume'] = counterObj.wholesale_volume
                    innerDict['retailVolume'] = counterObj.retail_volume
                    innerDict['status'] = counterObj.status
                    innerDict['createdOn'] = counterObj.created_on.strftime(
                        '%d %b %Y %H:%M:%S')
                    innerDict['createdBy'] = str(
                        counterObj.created_by) if counterObj.created_by else ""
                    innerDict['updatedOn'] = counterObj.updated_on.strftime(
                        '%d %b %Y %H:%M:%S')
                    innerDict['updatedBy'] = str(
                        counterObj.updated_by) if counterObj.updated_by else ""
                    finalResponseList.append(innerDict)
                log.info("Final response data list ready to return, finalResponseList: " +
                         str(len(finalResponseList)))
                return JsonResponse({"data": finalResponseList})
            return JsonResponse(prepare_response(message="No Data exist for the selected filter.", status="SUCCESS", success=True, info="No Data exist for the selected filter."))
        return JsonResponse(prepare_response(message="Only POST method allowed.", status="FAILURE", success=False, info="Only POST method allowed.", responseCode=5003))
    except Exception as ex:
        import traceback
        print(traceback.format_exc())
        log.error(
            "Exception occured while getting outlets data by filter, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while getting outlets data by filter, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again.", responseCode=5001))


def show_page_to_create_new_counter(request, emp_id, lat_long=""):
    if emp_id:
        try:
            user_object = UserMaster.objects.get(
                emp_id=emp_id)
            if not user_object.has_perm('app.has_so_permission'):
                print("EMP Id : " + str(emp_id) + " is not Authorized.")
                log.error("EMP Id : " + str(emp_id) +
                          " is not Authorized.")
                return redirect('/home/')
        except:
            log.error("No data found for emp_id: " + str(emp_id))
            return redirect('/home/')
        form1 = CreateNewCounterForm()
        form1.fields['emp_id'].initial = user_object.emp_id

        states_qs = StateMaster.objects.filter(
            region=user_object.region.region_id).order_by('state')
        states = [("", "--SELECT--")]
        for x in states_qs:
            states.append((x.state_id, x.state))
        form1.fields['state'].choices = states

        district_qs = DistrictMaster.objects.filter(
            state__region=user_object.region.region_id).order_by('district')
        districts = [("", "--SELECT--")]
        for x in district_qs:
            districts.append((x.district_id, x.district))
        form1.fields['district'].choices = districts

        city_qs = CityMaster.objects.filter(
            district__state__region=user_object.region.region_id).order_by('city')
        cities = [("", "--SELECT--")]
        for x in city_qs:
            cities.append((x.city_id, x.city))
        form1.fields['city'].choices = cities

        block_taluka_qs = BlockTalukaMaster.objects.filter(
            district__state__region=user_object.region.region_id).order_by('block_taluka')
        block_talukas = [("", "--SELECT--")]
        for x in block_taluka_qs:
            block_talukas.append((x.block_taluka_id, x.block_taluka))
        form1.fields['block_taluka_mandal'].choices = block_talukas

        form1.fields['so_code'].choices = [("", "--SELECT--")]

        form1.fields['asm'].choices = [("", "--SELECT--")]

        form1.fields['rsm'].choices = [("", "--SELECT--")]

        form1.fields['coach'].choices = [("", "--SELECT--")]

        form1.fields['zsm_state_head'].choices = [("", "--SELECT--")]

        form1.fields['cluster_head'].choices = [("", "--SELECT--")]

        if lat_long:
            form1.fields['geo_location'].initial = lat_long
        log.info("Successfully returned to new-counter with emp_id: " + str(emp_id))
        return render(request, 'createNewCounter.html', {'form': form1})
    else:
        log.error("No emp_id found in request.")
        return redirect('/home/')


@csrf_exempt
def show_page_to_create_new_counter_with_emp_id(request, emp_id):
    log.info("show_page_to_create_new_counter_with_emp_id of views, Emp Id: " +
             str(emp_id) + ", Request " + str(request))
    return show_page_to_create_new_counter(request, emp_id)


@csrf_exempt
def show_page_to_create_new_counter_with_emp_id_lat(request, emp_id, lat):
    log.info("show_page_to_create_new_counter_with_emp_id_lat of views, Emp Id: " +
             str(emp_id) + ", Latitude: " + str(lat) + ", Request " + str(request))
    return redirect('/new-counter/' + emp_id + '/')


@csrf_exempt
def show_page_to_create_new_counter_with_emp_id_lat_long(request, emp_id, lat, long):
    log.info("show_page_to_create_new_counter_with_emp_id_lat of views, Emp Id: " + str(emp_id) +
             ", Latitude: " + str(lat) + ", Longitude: " + str(long) + ", Request " + str(request))
    try:
        latitude = float(lat)
        longitude = float(long)
        lat_long = str(latitude) + ", " + str(longitude)
        return show_page_to_create_new_counter(request, emp_id, lat_long)
    except:
        log.error("Latitude and longitude is/are not of float type.")
        return redirect('/new-counter/' + emp_id + '/')


@csrf_exempt
def create_new_counter(request):
    log.info("save_page_1 of views, Request " + str(request))
    # import pdb
    # pdb.set_trace()
    form = CreateNewCounterForm(request.POST or None)
    if form.is_valid():
        user_object = UserMaster.objects.get(
            emp_id=form.cleaned_data['emp_id'])
        if user_object.has_perm('app.has_so_permission'):
            geoLocation= None
            latitude = 0.0
            longitude = 0.0
            if form.cleaned_data['geo_location']:
                try:
                    complete_location = str(
                        form.cleaned_data['geo_location']).split(",")
                    latitude = float(str(complete_location[0]).strip()) if len(
                        complete_location) > 0 else None
                    longitude = float(str(complete_location[1]).strip()) if len(
                        complete_location) > 1 else None
                    if latitude and longitude and int(latitude) != 0 and int(longitude) != 0:
                        geoLocation = str(latitude) + ", " + str(longitude)
                except Exception as ex:
                    log.error("Exception occured while formatting geo location, Error msg: " + str(ex))
            
            
            counter_object = OutletDetails.objects.create(
                counter_name=form.cleaned_data['counter_name'],
                phone_number=form.cleaned_data['phone_number'],
                email_id=form.cleaned_data['email_id'],
                region=user_object.region,
                state=StateMaster.objects.get(
                    state_id=form.cleaned_data['state']),
                district=DistrictMaster.objects.get(
                    district_id=form.cleaned_data['district']),
                block_taluka_mandal=BlockTalukaMaster.objects.get(
                    block_taluka_id=form.cleaned_data['block_taluka_mandal']),
                city=CityMaster.objects.get(city_id=form.cleaned_data['city']),
                geo_location=form.cleaned_data['geo_location'],
                counter_type=form.cleaned_data['counter_type'],
                dob=form.cleaned_data['dob'],
                comment=form.cleaned_data['comment'],
                unit=form.cleaned_data['unit'],
                total_potential=form.cleaned_data['total_potential'],
                wholesale_volume=form.cleaned_data['wholesale_volume'],
                retail_volume=form.cleaned_data['retail_volume'],
                is_cement_counter=form.cleaned_data['is_cement_counter'],
                updated_by=user_object,
                created_by=user_object)
            sales_heirarchy_inserted = False
            if form.cleaned_data['so_code']:
                so_user_obj = UserMaster.objects.get(
                    emp_id=form.cleaned_data['so_code'])
                counter_object.so_code = so_user_obj
                sales_heirarchy_inserted = True
            if form.cleaned_data['asm']:
                asm_user_obj = UserMaster.objects.get(
                    emp_id=form.cleaned_data['asm'])
                counter_object.asm = asm_user_obj
                sales_heirarchy_inserted = True
            if form.cleaned_data['rsm']:
                rsm_user_obj = UserMaster.objects.get(
                    emp_id=form.cleaned_data['rsm'])
                counter_object.rsm = rsm_user_obj
                sales_heirarchy_inserted = True
            if form.cleaned_data['coach']:
                coach_user_obj = UserMaster.objects.get(
                    emp_id=form.cleaned_data['coach'])
                counter_object.coach = coach_user_obj
                sales_heirarchy_inserted = True
            if form.cleaned_data['zsm_state_head']:
                zsm_state_head_user_obj = UserMaster.objects.get(
                    emp_id=form.cleaned_data['zsm_state_head'])
                counter_object.zsm_state_head = zsm_state_head_user_obj
                sales_heirarchy_inserted = True
            if form.cleaned_data['cluster_head']:
                cluster_head_user_obj = UserMaster.objects.get(
                    emp_id=form.cleaned_data['cluster_head'])
                counter_object.cluster_head = cluster_head_user_obj
                sales_heirarchy_inserted = True
            if sales_heirarchy_inserted:
                counter_object.save()
            log.info("Couter data created in OutletDetails against Emp Id: " +
                     str(form.cleaned_data['emp_id']))
            log.info("Successfully created new counter")
            if geoLocation:
                try:
                    GeoLocationMaster.objects.create(
                        dap=counter_object, latitude=latitude, longitude=longitude, updated_by=user_object, created_by=user_object)
                    log.info("GEO location inserted for DAP id: " + str(counter_object.dap_id) + ", Geo Location enetered are latitue: " + str(
                        latitude) + " and longitude: " + str(longitude) + ".")
                except Exception as ex:
                    log.error("Exception occured while inserting geo location in GeoLoationMaster for dap id: " +
                              str(counter_object.dap_id) + ", Error msg: " + str(ex))
            return render(request, 'end.html')
        else:
            log.error("Not authorized to create new counter.")
            return JsonResponse(prepare_response(message="Not authorized to create new counter.", status="FAILURE", success=False, info="Not authorized to create new counter."))
    log.error("Form Validation Failed for save_page_1 of views.")
    return redirect('/new-counter/' + str(form['so_code'].value()) + '/')


def get_all_states_under_region(request):
    log.info("get_all_states_under_region of views, Request " + str(request))
    states = {}
    try:
        state_qs = StateMaster.objects.filter(
            region=request.GET['region_id']).order_by('state').all()
        for state in state_qs:
            states[state.state_id] = state.state
        log.info("Successfully returned with states: " + str(states))
    except Exception as ex:
        log.error(
            "Exception occured while fething all sates under region, Error msg: " + str(ex))
    finally:
        data = {
            'state_data': states
        }
        return JsonResponse(data)


def get_all_districts_under_state(request):
    log.info("get_all_districts_under_state of views, Request " + str(request))
    districts = {}
    try:
        district_qs = DistrictMaster.objects.filter(
            state=request.GET['state_id']).order_by('district').all()
        for district in district_qs:
            districts[district.district_id] = district.district
        log.info("Successfully returned with districts: " + str(districts))
    except Exception as ex:
        log.error(
            "Exception occured while fething all districts under state, Error msg: " + str(ex))
    finally:
        data = {
            'district_data': districts
        }
        return JsonResponse(data)


def get_all_cities_and_block_taluka_under_district(request):
    log.info(
        "get_all_cities_and_block_taluka_under_district of views, Request " + str(request))
    cities = {}
    block_taluka_list = {}
    try:
        city_qs = CityMaster.objects.filter(
            district=request.GET['district_id']).order_by('city').all()
        block_taluka_qs = BlockTalukaMaster.objects.filter(
            district=request.GET['district_id']).order_by('block_taluka').all()
        for city in city_qs:
            cities[city.city_id] = city.city
        for block_taluka in block_taluka_qs:
            block_taluka_list[block_taluka.block_taluka_id] = block_taluka.block_taluka
        log.info("Successfully returned with cities and block taluka, cities: " +
                 str(cities) + ", block taluka list: " + str(block_taluka_list))
    except Exception as ex:
        log.error(
            "Exception occured while fething all cities and block taluka under district, Error msg: " + str(ex))
    finally:
        data = {
            'city_data': cities,
            'block_taluka': block_taluka_list
        }
        return JsonResponse(data)


def get_all_sales_heirarchy_under_district(request):
    log.info(
        "get_all_sales_heirarchy_under_district of views, Request " + str(request))
    so_list = {}
    asm_list = {}
    rsm_list = {}
    coach_list = {}
    zsm_state_head_list = {}
    cluster_head_list = {}
    try:
        outletDetails_qs = OutletDetails.objects.filter(
            district=request.GET['district_id'])
        outletDetails_so_qs = outletDetails_qs.values_list(
            'so_code').distinct()
        outletDetails_asm_qs = outletDetails_qs.values_list('asm').distinct()
        outletDetails_rsm_qs = outletDetails_qs.values_list('rsm').distinct()
        outletDetails_coach_qs = outletDetails_qs.values_list(
            'coach').distinct()
        outletDetails_zsm_state_head_qs = outletDetails_qs.values_list(
            'zsm_state_head').distinct()
        outletDetails_cluster_head_qs = outletDetails_qs.values_list(
            'cluster_head').distinct()
        userMaster_so_qs = UserMaster.objects.filter(
            emp_id__in=outletDetails_so_qs).order_by('emp_name')
        userMaster_asm_qs = UserMaster.objects.filter(
            emp_id__in=outletDetails_asm_qs).order_by('emp_name')
        userMaster_rsm_qs = UserMaster.objects.filter(
            emp_id__in=outletDetails_rsm_qs).order_by('emp_name')
        userMaster_coach_qs = UserMaster.objects.filter(
            emp_id__in=outletDetails_coach_qs).order_by('emp_name')
        userMaster_zsm_state_head_qs = UserMaster.objects.filter(
            emp_id__in=outletDetails_zsm_state_head_qs).order_by('emp_name')
        userMaster_cluster_head_qs = UserMaster.objects.filter(
            emp_id__in=outletDetails_cluster_head_qs).order_by('emp_name')
        for userMaster_so in userMaster_so_qs:
            so_list[userMaster_so.emp_id] = userMaster_so.emp_name
        for userMaster_asm in userMaster_asm_qs:
            asm_list[userMaster_asm.emp_id] = userMaster_asm.emp_name
        for userMaster_rsm in userMaster_rsm_qs:
            rsm_list[userMaster_rsm.emp_id] = userMaster_rsm.emp_name
        for userMaster_coach in userMaster_coach_qs:
            coach_list[userMaster_coach.emp_id] = userMaster_coach.emp_name
        for userMaster_zsm_state_head in userMaster_zsm_state_head_qs:
            zsm_state_head_list[userMaster_zsm_state_head.emp_id] = userMaster_zsm_state_head.emp_name
        for userMaster_cluster_head in userMaster_cluster_head_qs:
            cluster_head_list[userMaster_cluster_head.emp_id] = userMaster_cluster_head.emp_name
        log.info("Successfully returned with sales heirarcy data, \nso: " + str(so_list) + ", \nasm: " + str(asm_list) + ", \nrsm: " + str(rsm_list) +
                 ", \ncoach: " + str(coach_list) + ", \nzsm_state_head: " + str(zsm_state_head_list) + ", \ncluster_head: " + str(cluster_head_list))
    except Exception as ex:
        log.error(
            "Exception occured while fething all cities and block taluka under district, Error msg: " + str(ex))
    finally:
        data = {
            'so_data': so_list,
            'asm_data': asm_list,
            'rsm_data': rsm_list,
            'coach_data': coach_list,
            'zsm_state_head_data': zsm_state_head_list,
            'cluster_head_data': cluster_head_list
        }
        return JsonResponse(data)


@csrf_exempt
def reset_password_for_all_users(request):
    log.info("In reset_password_for_all_users, Request " + str(request))
    try:
        if request.headers['Authorization'] != 'TokenToSetDefaultPasswordForAllUsers':
            log.error("Authentication failed.")
            return JsonResponse(prepare_response(message="Authentication Failed.", status="FAILURE", success=False, info="Authentication Failed.", responseCode=401))
        userMasterObjects = UserMaster.objects.filter(is_superuser=0)
        updated_count = 0
        for userMasterObject in userMasterObjects:
            userMasterObject.set_password(userMasterObject.emp_phone_num)
            userMasterObject.save()
            updated_count = updated_count + 1
            log.info("Password of user with emp_id: " + str(userMasterObject.emp_id) +
                     " has been updated with his phone number " + str(userMasterObject.emp_phone_num) + " successfully.")
        res = prepare_response(message="Password of all users has been updated with there phone number successfully.",
                               status="SUCCESS", success=True, info="Password updated successfully.")
        res['updated_count'] = updated_count
        return JsonResponse(res)
    except Exception as ex:
        log.error(
            "Exception occured while updating password for all users, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while updating password for all users, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again after sometime.", responseCode=5001))


@csrf_exempt
def reset_password_of_particular_user(request, emp_id):
    log.info("In reset_password_of_particular_user, Request " + str(request))
    try:
        if request.headers['Authorization'] != 'TokenToResetPassword':
            log.error("Authentication failed.")
            return JsonResponse(prepare_response(message="Authentication Failed.", status="FAILURE", success=False, info="Authentication Failed.", responseCode=401))
        userMasterObject = UserMaster.objects.get(emp_id=emp_id)
        userMasterObject.set_password(userMasterObject.emp_phone_num)
        userMasterObject.save()
        log.info("Password of user with emp_id: " + str(userMasterObject.emp_id) +
                 " has been updated with his phone number " + str(userMasterObject.emp_phone_num) + " successfully.")
        msg = "Password of user with emp_id: " + \
            str(userMasterObject.emp_id) + " has been updated with his phone number " + \
            str(userMasterObject.emp_phone_num) + " successfully."
        return JsonResponse(prepare_response(message=msg, status="SUCCESS", success=True, info="Password updated successfully."))
    except Exception as ex:
        log.error(
            "Exception occured while reset password of particular user, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while reset password of particular user, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again after sometime.", responseCode=5001))


@csrf_exempt
def create_group_by_designation(request):
    log.info("In create_group_by_designation, Request " + str(request))
    try:
        if request.headers['Authorization'] != 'TokenToCreateGroup':
            log.error("Authentication failed.")
            return JsonResponse(prepare_response(message="Authentication Failed.", status="FAILURE", success=False, info="Authentication Failed.", responseCode=401))
        designations = DesignationMaster.objects.all()
        for designation in designations:
            if not designation.designation == 'SUPERUSER':
                Group.objects.create(name=designation.designation)
                log.info("Group: " + str(designation.designation) +
                         " has been created successfully.")
        return JsonResponse(prepare_response(message="Groups has been created successfully.",
                                             status="SUCCESS", success=True, info="Groups has been created successfully."))
    except Exception as ex:
        log.error(
            "Exception occured while creating groups, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while creating groups, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again after sometime.", responseCode=5001))


@csrf_exempt
def set_group_to_all_users_by_designation(request):
    log.info("In set_group_to_all_users_by_designation, Request " + str(request))
    try:
        if request.headers['Authorization'] != 'TokenToAssignGroupToAllUsers':
            log.error("Authentication failed.")
            return JsonResponse(prepare_response(message="Authentication Failed.", status="FAILURE", success=False, info="Authentication Failed.", responseCode=401))
        designations = DesignationMaster.objects.all()
        groups = {}
        for designation in designations:
            if not designation.designation == 'SUPERUSER':
                groups[designation.designation] = Group.objects.get(
                    name=designation.designation)
        userMasterObjects = UserMaster.objects.filter(is_superuser=0)
        updated_count = 0
        for userMasterObject in userMasterObjects:
            userMasterObject.groups.add(
                groups[userMasterObject.designation.designation])
            userMasterObject.save()
            updated_count = updated_count + 1
            log.info("Group to user with emp_id: " +
                     str(userMasterObject.emp_id) + " has been assigned successfully.")
        res = prepare_response(message="Group to all users has been assigned successfully.",
                               status="SUCCESS", success=True, info="Group assigned successfully.")
        res['updated_count'] = updated_count
        return JsonResponse(res)
    except Exception as ex:
        log.error(
            "Exception occured while assigning group to all users, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while assigning group to all users, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again after sometime.", responseCode=5001))


@csrf_exempt
def set_permission_to_all_users_by_counter_mapped(request):
    log.info(
        "In set_permission_to_all_users_by_counter_mapped, Request " + str(request))
    try:
        if request.headers['Authorization'] != 'TokenToAssignPermissionToAllUsers':
            log.error("Authentication failed.")
            return JsonResponse(prepare_response(message="Authentication Failed.", status="FAILURE", success=False, info="Authentication Failed.", responseCode=401))
        so_permission = Permission.objects.get(name='Has SO Permission')
        asm_permission = Permission.objects.get(name='Has ASM Permission')
        rsm_permission = Permission.objects.get(name='Has RSM Permission')
        zsm_state_head_permission = Permission.objects.get(
            name='Has ZSM or STATE HEAD Permission')
        coach_permission = Permission.objects.get(
            name='Has COACH HEAD Permission')
        cluster_head_permission = Permission.objects.get(
            name='Has CLUSTER HEAD Permission')
        userMasterObjects = UserMaster.objects.filter(Q(designation='SO') | Q(designation='ASM') | Q(designation='RSM') | Q(
            designation='STATE_HEAD') | Q(designation='COACH_HEAD') | Q(designation='CLUSTER_HEAD')).distinct()
        updated_count = 0
        for userMasterObject in userMasterObjects:
            so_outlets = OutletDetails.objects.filter(
                so_code=userMasterObject.emp_id).count()
            asm_outlets = OutletDetails.objects.filter(
                asm=userMasterObject.emp_id).count()
            rsm_outlets = OutletDetails.objects.filter(
                rsm=userMasterObject.emp_id).count()
            coach_outlets = OutletDetails.objects.filter(
                coach=userMasterObject.emp_id).count()
            zsm_state_head_outlets = OutletDetails.objects.filter(
                zsm_state_head=userMasterObject.emp_id).count()
            cluster_head_outlets = OutletDetails.objects.filter(
                cluster_head=userMasterObject.emp_id).count()
            changed = False
            if so_outlets != 0:
                changed = True
                userMasterObject.user_permissions.add(so_permission)
            if asm_outlets != 0:
                changed = True
                userMasterObject.user_permissions.add(asm_permission)
            if rsm_outlets != 0:
                changed = True
                userMasterObject.user_permissions.add(rsm_permission)
            if coach_outlets != 0:
                changed = True
                userMasterObject.user_permissions.add(coach_permission)
            if zsm_state_head_outlets != 0:
                changed = True
                userMasterObject.user_permissions.add(
                    zsm_state_head_permission)
            if cluster_head_outlets != 0:
                changed = True
                userMasterObject.user_permissions.add(cluster_head_permission)
            if changed:
                userMasterObject.save()
                updated_count = updated_count + 1
                log.info("Permission/s to user with emp_id: " +
                         str(userMasterObject.emp_id) + " has been assigned successfully.")
        res = prepare_response(message="Permission to all users has been assigned successfully.",
                               status="SUCCESS", success=True, info="Permission assigned successfully.")
        res['updated_count'] = updated_count
        return JsonResponse(res)
    except Exception as ex:
        log.error(
            "Exception occured while assigning permission to all users, Error msg: " + str(ex))
        return JsonResponse(prepare_response(message="Exception occured while assigning permission to all users, Error msg: " + str(ex), status="FAILURE", success=False, info="Something went wrong, Please try again after sometime.", responseCode=5001))
