from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('health-check/', views.health_check_api, name='health_check_api'),
    path('home/', views.home, name='home'),
    path('ajax/validate_counter_by_dap_id/', views.validate_counter_by_dap_id, name='validate_counter_by_dap_id'),

    path('ajax/all-states-under-region/', views.get_all_states_under_region, name='get_all_states_under_region'),
    path('ajax/all-districts-under-state/', views.get_all_districts_under_state, name='get_all_districts_under_state'),
    path('ajax/all-cities-block-taluka-under-district/', views.get_all_cities_and_block_taluka_under_district, name='get_all_cities_and_block_taluka_under_district'),
    path('ajax/all-sales-heirarchy-under-district/', views.get_all_sales_heirarchy_under_district, name='get_all_sales_heirarchy_under_district'),

    path('page-1/', views.home, name='home'),
    path('e-page-1/<so_code>/<dap_id>/<lat_long>/', views.edit_page_1_with_latlong, name='show_page_1'),
    path('e-page-1/<so_code>/<dap_id>/', views.edit_page_1_without_latlong, name='show_page_1'),
    path('page-1/<so_code>/', views.show_page_1_with_so_code, name='show_page_1'),
    path('page-1/<so_code>/<lat>/', views.show_page_1_with_so_code_lat, name='show_page_1'),
    path('page-1/<so_code>/<lat>/<long>/', views.show_page_1_with_so_code_lat_long, name='show_page_1'),
    path('page-2/<dap_id>/', views.show_page_2, name='show_page_2'),
    # url(r'^page-1/(?P<so_code>\d+)/$', views.show_page_1, name='show_page_1'),

    path('page-2/', views.save_page_1, name='save_page_1'),
    path('page-3/', views.save_page_2, name='save_page_2'),
    path('preview/', views.save_page_3, name='save_page_3'),
    path('submit/', views.submit, name='submit'),

    path('generate-report/', views.generate_report, name='generate_report'),
    path('download-report/', views.download_report, name='download_report'),

    path('get-outlet-data/', views.get_outlets_data_by_filter, name='get_outlets_data_by_filter'),
    
    path('update_is_verified/<dap_id>/', views.update_is_verified, name='update_is_verified'),
    path('update_is_sap_code_verified/<dap_id>/', views.update_is_sap_code_verified, name='update_is_sap_code_verified'),

    path('reset-password-all-users/', views.reset_password_for_all_users, name='reset_password_for_all_users'),
    path('reset-password/<emp_id>/', views.reset_password_of_particular_user, name='reset_password_of_particular_user'),
    path('create-groups/', views.create_group_by_designation, name='create_group_by_designation'),
    path('assign-group-all-users/', views.set_group_to_all_users_by_designation, name='set_group_to_all_users_by_designation'),
    path('assign-permission-all-users/', views.set_permission_to_all_users_by_counter_mapped, name='set_permission_to_all_users_by_counter_mapped'),

    path('new-counter/<emp_id>/', views.show_page_to_create_new_counter_with_emp_id, name='show_page_to_create_new_counter_with_emp_id'),
    path('new-counter/<emp_id>/<lat>/', views.show_page_to_create_new_counter_with_emp_id_lat, name='show_page_to_create_new_counter_with_emp_id_lat'),
    path('new-counter/<emp_id>/<lat>/<long>/', views.show_page_to_create_new_counter_with_emp_id_lat_long, name='show_page_to_create_new_counter_with_emp_id_lat_long'),
    path('submit-new-counter/', views.create_new_counter, name='create_new_counter')
    
]