from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from smart_selects.db_fields import ChainedForeignKey


class DesignationMaster(models.Model):
    designation = models.CharField(max_length=20, primary_key=True)
    designation_name = models.CharField(max_length=30, null=True)
    priority = models.PositiveSmallIntegerField(default=5, validators=[MinValueValidator(1), MaxValueValidator(10)])
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.designation_name)

    class Meta:
        verbose_name = 'Designation Master'
        verbose_name_plural = 'Designation Master'



class RegionMaster(models.Model):
    def increment_display_number():
        last_display = RegionMaster.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    def increment_region_number():
        last_region = RegionMaster.objects.all().order_by('region_id').last()
        if not last_region:
            return 101
        old_region_id = last_region.region_id
        new_region_id = old_region_id + 1
        return new_region_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    region_id = models.IntegerField(default=increment_region_number, editable=False, primary_key=True)
    region = models.CharField(max_length=200)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.region)

    class Meta: 
          
        permissions = (
            ('has_current_region_access_only_regionmaster', 'Has Current Region Access Only'),
            ('has_all_regions_access_regionmaster', 'Has All Regions Access'),
        )

    class Meta:
        verbose_name = 'Region Master'
        verbose_name_plural = 'Region Master'

class StateMaster(models.Model):
    def increment_display_number():
        last_display = StateMaster.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    state_id = models.IntegerField(primary_key=True, editable=False)
    state = models.CharField(max_length=200)
    region = models.ForeignKey(RegionMaster, default=None, on_delete=models.CASCADE)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.state)

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_state = self.__class__.objects.filter(region=self.region).order_by('state_id').last()
            if last_state is not None:
                old_state_id = last_state.state_id
                old_state_id_int = int(str(old_state_id)[3:])
                new_state_id_int = old_state_id_int + 1
                self.state_id = int(str(self.region.region_id) + str(new_state_id_int).zfill(3) )
            else:
                self.state_id = int(str(self.region.region_id) + '001')
        super(StateMaster, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'State Master'
        verbose_name_plural = 'State Master'

class DistrictMaster(models.Model):
    def increment_display_number():
        last_display = DistrictMaster.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    district_id = models.IntegerField(primary_key=True, editable=False)
    district = models.CharField(max_length=200)
    state = models.ForeignKey(StateMaster, on_delete=models.CASCADE)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.district)

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_district = self.__class__.objects.filter(state=self.state).order_by('district_id').last()
            if last_district is not None:
                old_district_id = last_district.district_id
                old_district_id_int = int(str(old_district_id)[6:])
                new_district_id_int = old_district_id_int + 1
                self.district_id = int(str(self.state.state_id) + str(new_district_id_int).zfill(3) )
            else:
                self.district_id = int(str(self.state.state_id) + '001')
        super(DistrictMaster, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'District Master'
        verbose_name_plural = 'District Master'

class CityMaster(models.Model):
    def increment_display_number():
        last_display = CityMaster.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    city_id = models.BigIntegerField(primary_key=True, editable=False)
    city = models.CharField(max_length=200)
    district = models.ForeignKey(DistrictMaster, default=None, on_delete=models.CASCADE, related_name='city2district')
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.city)

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_city = self.__class__.objects.filter(district=self.district).order_by('city_id').last()
            if last_city is not None:
                old_city_id = last_city.city_id
                old_city_id_int = int(str(old_city_id)[9:])
                new_city_id_int = old_city_id_int + 1
                self.city_id = int(str(self.district.district_id) + str(new_city_id_int).zfill(4) )
            else:
                self.city_id = int(str(self.district.district_id) + '0001')
        super(CityMaster, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'City Master'
        verbose_name_plural = 'City Master'

class BlockTalukaMaster(models.Model):
    def increment_display_number():
        last_display = BlockTalukaMaster.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    block_taluka_id = models.BigIntegerField(primary_key=True, editable=False)
    block_taluka = models.CharField(max_length=200)
    district = models.ForeignKey(DistrictMaster, null=True, on_delete=models.CASCADE, related_name='block2district')
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.block_taluka)

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_block_taluka = self.__class__.objects.filter(district=self.district).order_by('block_taluka_id').last()
            if last_block_taluka is not None:
                old_block_taluka_id = last_block_taluka.block_taluka_id
                old_block_taluka_id_int = int(str(old_block_taluka_id)[9:])
                new_block_taluka_id_int = old_block_taluka_id_int + 1
                self.block_taluka_id = int(str(self.district.district_id) + str(new_block_taluka_id_int).zfill(4) )
            else:
                self.block_taluka_id = int(str(self.district.district_id) + '0001')
        super(BlockTalukaMaster, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Block Taluka Mandal Master'
        verbose_name_plural = 'Block Taluka Mandal Master'

class UserMaster(AbstractUser):
    emp_id = models.CharField(max_length=20, primary_key=True)
    emp_name = models.CharField(max_length=200)
    emp_phone_num = models.BigIntegerField(null=True, verbose_name='Mobile number', unique=True,)
    email_id = models.CharField(max_length=200, null=True, blank=True)
    designation = models.ForeignKey(DesignationMaster, default=None, on_delete=models.CASCADE)
    region = models.ForeignKey(RegionMaster, default=None, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.CharField(max_length=200, null=True, blank=True)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.CharField(max_length=200, null=True, blank=True)
    
    USERNAME_FIELD = 'emp_phone_num'

    def __str__(self):
        return str(self.emp_name)

    class Meta:
        verbose_name = 'User Master'
        verbose_name_plural = 'User Master'

class OutletDetails(models.Model):

    ACTIVE, INACTIVE, ARCHIVE = 'ACTIVE', 'INACTIVE', 'ARCHIVE'

    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (INACTIVE, 'In active'),
        (ARCHIVE, 'Archive')
    )

    dap_id = models.AutoField(primary_key=True)
    is_verified = models.BooleanField(default=False)
    verified_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='verified_by')
    is_sap_code_verified = models.BooleanField(default=False)
    sap_code_verified_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='sap_code_verified_by')
    so_code = models.ForeignKey(UserMaster, on_delete=models.PROTECT, verbose_name='Sales Officer', null=True, blank=True, related_name='so2emp', )
    sap_code = models.CharField(max_length=200, unique=True, null=True, blank=True)
    counter_name = models.CharField(max_length=200, null=True, blank=True)
    email_id = models.CharField(max_length=200, null=True, blank=True)
    region = models.ForeignKey(RegionMaster, on_delete=models.PROTECT)
    state = ChainedForeignKey(
        StateMaster,
        chained_field='region',
        chained_model_field='region',
        show_all=False,
        auto_choose=True,
        sort=True, 
        on_delete=models.PROTECT)
    district = ChainedForeignKey(
        DistrictMaster,
        chained_field='state',
        chained_model_field='state',
        show_all=False,
        auto_choose=True,
        sort=True, 
        on_delete=models.PROTECT)
    city = ChainedForeignKey(
        CityMaster,
        chained_field='district',
        chained_model_field='district',
        show_all=False,
        auto_choose=True,
        sort=True, 
        on_delete=models.PROTECT)
    block_taluka_mandal = ChainedForeignKey(
        BlockTalukaMaster,
        chained_field='district',
        chained_model_field='district',
        show_all=False,
        auto_choose=True,
        sort=True, 
        on_delete=models.PROTECT)
    phone_number = models.BigIntegerField(null=True, verbose_name='Mobile number')
    geo_location = models.CharField(default=0, max_length=200, null=True, blank=True)
    is_cement_counter = models.CharField(max_length=5, null=True, blank=True)
    counter_type = models.CharField(max_length=200, null=True, blank=True)
    dob = models.DateField(default=None, null=True, blank=True)
    comment = models.TextField(default=None, max_length=200, null=True, blank=True)
    unit = models.CharField(max_length=200, null=True, blank=True)
    asm = models.ForeignKey(UserMaster, on_delete=models.PROTECT, null=True, blank=True, related_name='asm2emp',)
    rsm = models.ForeignKey(UserMaster, on_delete=models.PROTECT, null=True, blank=True, related_name='rsm2emp',)
    coach = models.ForeignKey(UserMaster, on_delete=models.PROTECT, null=True, blank=True, related_name='coach2emp',)
    zsm_state_head = models.ForeignKey(UserMaster, on_delete=models.PROTECT, null=True, blank=True, related_name='state2emp',)
    cluster_head = models.ForeignKey(UserMaster, on_delete=models.PROTECT, null=True, blank=True, related_name='cluster2emp',)
    total_potential = models.CharField(default=0, max_length=100, null=True, blank=True)
    wholesale_volume = models.CharField(default=0, max_length=100, null=True, blank=True)
    retail_volume = models.CharField(default=0, max_length=100, null=True, blank=True)
    status = models.CharField(default=ACTIVE, choices=STATUS_CHOICES, max_length=12)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='OutletDetailsUpdatedBy')
    
    def __str__(self):
        return str(self.dap_id)

    class Meta: 
          
        permissions = (
            ('can_edit_sales_hierarchy_outletdetails', 'Can Edit Sales Hierarchy'),
            ('can_edit_basic_details_outletdetails', 'Can Edit Basic Details'),
            ('can_edit_geo_details_outletdetails', 'Can Edit GEO Details'),
            ('can_edit_potential_outletdetails', 'Can Edit Potential'),
            ('bulk_update_outletdetails', 'Can Update Bulk Sales Heirarchy'),
            ('verify_outletdetails', 'Can Verify Outlet'),
            ('bulk_verify_outletdetails', 'Can Verify Bulk Outlets'),
            ('bulk_archive_outletdetails', 'Can Archieve Bulk Outlets'),
            ('has_so_permission', 'Has SO Permission'),
            ('has_asm_permission', 'Has ASM Permission'),
            ('has_rsm_permission', 'Has RSM Permission'),
            ('has_zsm_state_head_permission', 'Has ZSM or STATE HEAD Permission'),
            ('has_sales_head_permission', 'Has SALES HEAD Permission'),
            ('has_senior_sales_head_permission', 'Has SENIOR SALES HEAD Permission'),
            ('has_coach_head_permission', 'Has COACH HEAD Permission'),
            ('has_cluster_head_permission', 'Has CLUSTER HEAD Permission'),
            ('has_regional_spoc_permission', 'Has REGIONAL SPOC Permission'),
            ('has_management_permission', 'Has MANAGEMENT Permission'),
        )

    class Meta:
        verbose_name = 'Outlet Details'
        verbose_name_plural = 'Outlet Details'

class OutletMaster(models.Model):

    GOOD, AVERAGE, POOR = 'Good', 'Average', 'Poor'

    REPUTATION_CHOICES=(
        ('Good','Good'),
        ('Average','Average'),
        ('Poor','Poor')
    )
    dap = models.ForeignKey(OutletDetails, on_delete=models.CASCADE, null=True)
    no_of_employees = models.FloatField(default=0, null=True, blank=True)
    no_of_delivery_vans = models.FloatField(default=0, null=True, blank=True)
    average_cement_stock = models.FloatField(default=0, null=True, blank=True)
    next_generation_in_business = models.CharField(default=0, max_length=200, null=True, blank=True)
    does_dealer_walk_the_market = models.CharField(default=0, max_length=200, null=True, blank=True)
    personal_investment_in_business = models.FloatField(default=0, null=True, blank=True)
    no_of_sub_dealers = models.FloatField(default=0, null=True, blank=True)
    non_cement_turnover = models.FloatField(default=0, null=True, blank=True)
    business_1 = models.CharField(max_length=200, null=True, blank=True)
    average_monthly_sales_1 = models.FloatField(default=0, null=True, blank=True)
    business_2 = models.CharField(max_length=200, null=True, blank=True)
    average_monthly_sales_2 = models.FloatField(default=0, null=True, blank=True)
    cnf_cfa_brand_names = models.CharField(max_length=200, null=True, blank=True)
    sp_brand_names = models.CharField(max_length=200, null=True, blank=True)
    transporter_brand_names = models.CharField(max_length=200, null=True, blank=True)
    indenting_agents_brand_names = models.CharField(max_length=200, null=True, blank=True)
    reputation_of_counter = models.CharField(default=GOOD, choices=REPUTATION_CHOICES, max_length=12)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='OutletMasterUpdatedBy')

    def __str__(self):
        return str(self.dap.dap_id)

    class Meta:
        verbose_name = 'Outlet Additional Info'
        verbose_name_plural = 'Outlet Additional Info'

class GeoLocationMaster(models.Model):
    def increment_display_number():
        last_display = GeoLocationMaster.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    dap = models.ForeignKey(OutletDetails, on_delete=models.CASCADE)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='GeoLocationMasterUpdatedBy')
    
    def __str__(self):
        return str(self.dap.dap_id)

    class Meta:
        verbose_name = 'GEO Location Master'
        verbose_name_plural = 'GEO Location Master'


class BrandsList(models.Model):
    def increment_display_number():
        last_display = BrandsList.objects.all().order_by('display_id').last()
        if not last_display:
            return 1
        old_display_id = last_display.display_id
        new_display_id = old_display_id + 1
        return new_display_id
    
    display_id = models.IntegerField(default=increment_display_number, editable=False)
    brand_id = models.CharField(max_length=20, primary_key=True, editable=False)
    brand_name = models.CharField(max_length=50)

    def __str__(self):
        return str(self.brand_name)

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_brand = self.__class__.objects.order_by('display_id').last()
            if last_brand is not None:
                old_brand_id = last_brand.brand_id
                old_brand_id_int = int(str(old_brand_id)[1:])
                new_brand_id_int = old_brand_id_int + 1
                self.brand_id = str("B" + str(new_brand_id_int).zfill(4))
            else:
                self.brand_id = "B0001"
        super(BrandsList, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Brands list Master'
        verbose_name_plural = 'Brands list Master'

class BrandMaster(models.Model):
    dap = models.ForeignKey(OutletDetails, on_delete=models.CASCADE, null=True)
    brand = models.ForeignKey(BrandsList, default='B0', on_delete=models.CASCADE, null=True)
    brand_name = models.CharField(max_length=200, null=True, blank=True)
    category = models.CharField(default=0, max_length=200, null=True, blank=True)
    total_potential = models.CharField(default=0, max_length=200, null=True, blank=True)
    total_wholesale_volume = models.CharField(default=0, max_length=200, null=True, blank=True)
    total_retail_volume = models.CharField(default=0, max_length=200, null=True, blank=True)
    wholesale_volume = models.CharField(default=0, max_length=200, null=True, blank=True)
    retail_volume = models.CharField(default=0, max_length=200, null=True, blank=True)
    opc = models.CharField(default=0, max_length=200, null=True, blank=True)
    ppc = models.CharField(default=0, max_length=200, null=True, blank=True)
    pcc = models.CharField(default=0, max_length=200, null=True, blank=True)
    psc = models.CharField(default=0, max_length=200, null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='BrandMasterUpdatedBy')
    
    def __str__(self):
        return str(self.dap.dap_id)

    class Meta:
        verbose_name = 'Counter wise Brands Potential'
        verbose_name_plural = 'Counter wise Brands Potential'


class TargetTypeMaster(models.Model):
    target_type_id = models.AutoField(primary_key=True)
    target_type = models.CharField(max_length=200)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.CharField(max_length=200, null=True, blank=True)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='TargetTypeMasterUpdatedBy')

    def __str__(self):
        return str(self.target_type_id)

    class Meta:
        verbose_name = 'Target Type Master'
        verbose_name_plural = 'Target Type Master'

class TargetMaster(models.Model):
    target_id = models.CharField(max_length=20, primary_key=True)
    emp = models.ForeignKey(UserMaster, on_delete=models.PROTECT, null=True, blank=True)
    target_type = models.ForeignKey(TargetTypeMaster, on_delete=models.CASCADE, null=True, blank=True)
    dap = models.ForeignKey(OutletDetails, on_delete=models.CASCADE, null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now)
    valid_from = models.DateTimeField(default=None)
    valid_to = models.DateTimeField(default=None)
    assigned_on = models.DateTimeField(default=None)
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, blank=True, related_name='TargetMasterUpdatedBy')

    def __str__(self):
        return str(self.target_id)

    class Meta:
        verbose_name = 'Target Master'
        verbose_name_plural = 'Target Master'