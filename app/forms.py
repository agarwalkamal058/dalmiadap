from django import forms
from .models import *
from phonenumber_field.modelfields import PhoneNumberField


class NoValidationChoice(forms.ChoiceField):
    def validate(self, value):
        pass

class HomeForm(forms.Form):
    so_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))

class Type1Page1Form(forms.Form):
    CHOICES=[('yes','YES'),('no','NO')]
    brandList = BrandsList.objects.all().order_by('brand_name')
    brand_name_list = [('', '-- Select Brand --')]
    for x in brandList:
        option = (x.brand_id, x.brand_name)
        brand_name_list.append(option)
    outlet_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}))
    counter_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}))
    so_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}))
    dap_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}))
    sap_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    state = forms.ChoiceField(choices=[(x.state_id, x.state) for x in StateMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    district = forms.ChoiceField(choices=[(x.district_id, x.district) for x in DistrictMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    city = forms.ChoiceField(choices=[(x.city_id, x.city) for x in CityMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    block_taluka_mandal = forms.ChoiceField(choices=[(x.block_taluka_id, x.block_taluka) for x in BlockTalukaMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Block/Taluka/Mandal'}))
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'type':'text', 'class': 'form-control form-control-sm', 'placeholder': 'Enter Phone no.', 'pattern': '[6-9]{1}[0-9]{9}', 'title': 'Please enter 10 digit phone no.', 'oninvalid': 'setCustomValidity("Please enter 10 digit phone no.")', 'oninput': 'this.setCustomValidity("")'}), max_length=10, min_length=10, required=True)
    geo_location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Latitute, Longitude'}), required=False)
    is_a_cement_counter = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}))
    no_of_employees = forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=True)
    no_of_delivery_vans = forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=True)
    average_cement_stock = forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=False)
    next_generation_in_business = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}))
    does_dealer_walk_the_market = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}))
    personal_investment_in_business = forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=False)
    no_of_sub_dealers = forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=True)
    dob = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control form-control-sm', 'placeholder':'Date of Birth'}),required=False)

    def __init__(self, *args, **kwargs):
        super(Type1Page1Form, self).__init__(*args, **kwargs)
        self.fields['outlet_name'].label = 'Outlet Name'
        self.fields['dap_id'].label = 'SAP Id'
        self.fields['phone_number'].label = 'Phone No.'
        self.fields['geo_location'].label = 'Geo Tag'
        self.fields['block_taluka_mandal'].label = 'Block/Taluka/Mandal'
        self.fields['no_of_employees'].label = 'No. of Employees'
        self.fields['no_of_delivery_vans'].label = 'No. of 3W/Truck/Tractor'
        self.fields['average_cement_stock'].label = 'Average cement stock (MT/Mth)'
        self.fields['personal_investment_in_business'].label = 'Personal investment in business (INR Lakhs)'
        self.fields['does_dealer_walk_the_market'].label = 'Does the dealer/sub-dealer go to the market'
        self.fields['dob'].label = 'Date of Birth (DOB)'

class Type1Page2Form(forms.Form):
    brandList = BrandsList.objects.all().order_by('brand_name')
    brand_name_list = [('', '-- Select Brand --')]
    for x in brandList:
        option = (x.brand_id, x.brand_name)
        brand_name_list.append(option)
    so_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}))
    dap_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}))
    is_a_cement_counter = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    CHOICES=[('yes','YES'),('no','NO')]
    any_non_cement_business = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}), required=False)
    non_cement_turnover = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=False)
    business_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Enter business category'}), required=False)
    average_monthly_sales_1 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=False)
    business_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Enter business category'}), required=False)
    average_monthly_sales_2 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=False)
    is_a_cnf_cfa = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    is_a_cnf_cfa_radio = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}), required=False)
    is_a_sp = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    is_a_sp_radio = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}), required=False)
    is_a_transporter = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    is_a_transporter_radio = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}), required=False)
    is_a_indenting_agent = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    is_a_indenting_agent_radio = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}), required=False)
    reputation_of_counter = forms.ChoiceField(choices=OutletMaster.REPUTATION_CHOICES, initial='Good', widget=forms.RadioSelect(attrs={'class': ''}), required=False)
    
    def __init__(self, *args, **kwargs):
        super(Type1Page2Form, self).__init__(*args, **kwargs)
        self.fields['any_non_cement_business'].label = 'Any Non Cement Business?'
        self.fields['non_cement_turnover'].label = 'Tot. Monthly Sales - other businesses (INR Cr.)'
        self.fields['average_monthly_sales_1'].label = 'Avg. Monthly sales (INR Cr.)'
        self.fields['average_monthly_sales_2'].label = 'Avg. Monthly sales (INR Cr.)'
        self.fields['is_a_cnf_cfa'].label = 'Is a CNF/CFA'
        self.fields['is_a_sp'].label = 'Is a SP'
        self.fields['is_a_transporter'].label = 'Is a Transporter'
        self.fields['is_a_indenting_agent'].label = 'Is an indenting agent'
        
class Type1Page3Form(forms.Form):
    so_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}))
    dap_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}))
    brandList = BrandsList.objects.all().order_by('brand_name')
    brand_name_list = [('', '-- Select Brand --')]
    for x in brandList:
        option = (x.brand_id, x.brand_name)
        brand_name_list.append(option)
    total_potential = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    wholesale_volume = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    retail_volume = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth', 'readonly': 'readonly'}))
    brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    wholesale_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    opc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    ppc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    pcc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    psc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    wholesale_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    opc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    ppc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    pcc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    psc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    wholesale_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    opc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    ppc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    pcc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    psc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    wholesale_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    opc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    ppc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    pcc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    psc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    wholesale_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    opc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    ppc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    pcc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    psc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}), required=False)
    other_brand_retailsale = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    other_brand_wholesale = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    comment = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Enter additional information here in <200 chars', 'rows': 5}), max_length=200, required=False)

    def __init__(self, *args, **kwargs):
        super(Type1Page3Form, self).__init__(*args, **kwargs)
        self.fields['total_potential'].label = 'Tot. Pot.(MT/Mth)'
        self.fields['wholesale_volume'].label = 'W/S Vol(MT/Mth)'
        self.fields['retail_volume'].label = 'Calculated Retail Vol(MT/Mth)'
        self.fields['brand_1'].label = 'Brand 1'
        self.fields['wholesale_brand_1'].label = 'Tot. W/S selected brand'
        self.fields['opc_brand_1'].label = 'OPC'
        self.fields['ppc_brand_1'].label = 'PPC'
        self.fields['pcc_brand_1'].label = 'PCC'
        self.fields['psc_brand_1'].label = 'PSC'
        self.fields['brand_2'].label = 'Brand 2'
        self.fields['wholesale_brand_2'].label = 'Tot. W/S selected brand'
        self.fields['opc_brand_2'].label = 'OPC'
        self.fields['ppc_brand_2'].label = 'PPC'
        self.fields['pcc_brand_2'].label = 'PCC'
        self.fields['psc_brand_2'].label = 'PSC'
        self.fields['brand_3'].label = 'Brand 3'
        self.fields['wholesale_brand_3'].label = 'Tot. W/S selected brand'
        self.fields['opc_brand_3'].label = 'OPC'
        self.fields['ppc_brand_3'].label = 'PPC'
        self.fields['pcc_brand_3'].label = 'PCC'
        self.fields['psc_brand_3'].label = 'PSC'
        self.fields['brand_4'].label = 'Brand 4'
        self.fields['wholesale_brand_4'].label = 'Tot. W/S selected brand'
        self.fields['opc_brand_4'].label = 'OPC'
        self.fields['ppc_brand_4'].label = 'PPC'
        self.fields['pcc_brand_4'].label = 'PCC'
        self.fields['psc_brand_4'].label = 'PSC'
        self.fields['brand_5'].label = 'Brand 5'
        self.fields['wholesale_brand_5'].label = 'Tot. W/S selected brand'
        self.fields['opc_brand_5'].label = 'OPC'
        self.fields['ppc_brand_5'].label = 'PPC'
        self.fields['pcc_brand_5'].label = 'PCC'
        self.fields['psc_brand_5'].label = 'PSC'

class Type1Page4PreviewForm(forms.Form):
    so_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    dap_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    counter_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    state = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    district = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    city = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    block_taluka_mandal = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    geo_location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    is_a_cement_counter = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    no_of_employees = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    no_of_delivery_vans = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    average_cement_stock = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    next_generation_in_business = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    does_dealer_walk_the_market = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    personal_investment_in_business = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    no_of_sub_dealers = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    any_non_cement_business = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    non_cement_turnover = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    business_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    average_monthly_sales_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    business_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    average_monthly_sales_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    is_a_cnf_cfa = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly', 'rows':2}), required=False)
    is_a_sp = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly', 'rows':2}), required=False)
    is_a_transporter = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly', 'rows':2}), required=False)
    is_a_indenting_agent = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly', 'rows':2}), required=False)
    reputation_of_counter = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    total_potential = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    wholesale_volume = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    retail_volume = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    wholesale_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    opc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    ppc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    pcc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    psc_brand_1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    wholesale_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    opc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    ppc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    pcc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    psc_brand_2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    wholesale_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    opc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    ppc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    pcc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    psc_brand_3 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    wholesale_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    opc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    ppc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    pcc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    psc_brand_4 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    wholesale_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    opc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    ppc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    pcc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    psc_brand_5 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    dob = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    comment = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'readonly': 'readonly'}), required=False)
    
    def __init__(self, *args, **kwargs):
        super(Type1Page4PreviewForm, self).__init__(*args, **kwargs)
        self.fields['counter_name'].label = 'Outlet Name'
        self.fields['phone_number'].label = 'Phone No.'
        self.fields['geo_location'].label = 'Geo Tag'
        self.fields['block_taluka_mandal'].label = 'Block/Taluka/Mandal'
        self.fields['no_of_employees'].label = 'No. of Employees'
        self.fields['no_of_delivery_vans'].label = 'No. of Delivery Vans'
        self.fields['does_dealer_walk_the_market'].label = 'Does the dealer/sub-dealer go to the market'
        self.fields['any_non_cement_business'].label = 'Any Non Cement Business?'
        self.fields['non_cement_turnover'].label = 'Total Avg. Monthly Sales from other businesses (INR Cr.)'
        self.fields['average_monthly_sales_1'].label = 'Avg. Monthly sales (in INR Cr.)'
        self.fields['average_monthly_sales_2'].label = 'Avg. Monthly sales (in INR Cr.)'
        self.fields['is_a_cnf_cfa'].label = 'Is a CNF/CFA'
        self.fields['is_a_sp'].label = 'Is a SP'
        self.fields['is_a_transporter'].label = 'Is a Transporter'
        self.fields['is_a_indenting_agent'].label = 'Is an indenting agent'
        self.fields['total_potential'].label = 'Total Potential'
        self.fields['wholesale_volume'].label = 'Wholesale Volume'
        self.fields['retail_volume'].label = 'Retail Volume'
        self.fields['brand_1'].label = 'Brand 1'
        self.fields['wholesale_brand_1'].label = 'Total Wholesale for selected brand'
        self.fields['opc_brand_1'].label = 'OPC'
        self.fields['ppc_brand_1'].label = 'PPC'
        self.fields['pcc_brand_1'].label = 'PCC'
        self.fields['psc_brand_1'].label = 'PSC'
        self.fields['brand_2'].label = 'Brand 2'
        self.fields['wholesale_brand_2'].label = 'Total Wholesale for selected brand'
        self.fields['opc_brand_2'].label = 'OPC'
        self.fields['ppc_brand_2'].label = 'PPC'
        self.fields['pcc_brand_2'].label = 'PCC'
        self.fields['psc_brand_2'].label = 'PSC'
        self.fields['brand_3'].label = 'Brand 3'
        self.fields['wholesale_brand_3'].label = 'Total Wholesale for selected brand'
        self.fields['opc_brand_3'].label = 'OPC'
        self.fields['ppc_brand_3'].label = 'PPC'
        self.fields['pcc_brand_3'].label = 'PCC'
        self.fields['psc_brand_3'].label = 'PSC'
        self.fields['brand_4'].label = 'Brand 4'
        self.fields['wholesale_brand_4'].label = 'Total Wholesale for selected brand'
        self.fields['opc_brand_4'].label = 'OPC'
        self.fields['ppc_brand_4'].label = 'PPC'
        self.fields['pcc_brand_4'].label = 'PCC'
        self.fields['psc_brand_4'].label = 'PSC'
        self.fields['brand_5'].label = 'Brand 5'
        self.fields['wholesale_brand_5'].label = 'Total Wholesale for selected brand'
        self.fields['opc_brand_5'].label = 'OPC'
        self.fields['ppc_brand_5'].label = 'PPC'
        self.fields['pcc_brand_5'].label = 'PCC'
        self.fields['psc_brand_5'].label = 'PSC'
        self.fields['dob'].label = 'Date of Birth (DOB)'

class EmptyChoiceField(forms.ChoiceField):
    def __init__(self, choices=(), empty_label=None, required=True, widget=None, label=None,
                 initial=None, help_text=None, *args, **kwargs):

        # prepend an empty label if it exists (and field is not required!)
        if not required and empty_label is not None:
            choices = tuple([(u'', empty_label)] + list(choices))

        super(EmptyChoiceField, self).__init__(choices=choices, required=required, widget=widget, label=label,
                                        initial=initial, help_text=help_text, *args, **kwargs)


class CreateNewCounterForm(forms.Form):
    CHOICES=[('yes','YES'),('no','NO')]

    emp_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}))
    counter_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'placeholder':'Enter Counter name'}))
    email_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'placeholder':'Eneter Email Id'}))
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'type':'text', 'class': 'form-control form-control-sm', 'placeholder': 'Enter Phone no.', 'pattern': '[6-9]{1}[0-9]{9}', 'title': 'Please enter 10 digit phone no.', 'oninvalid': 'setCustomValidity("Please enter 10 digit phone no.")', 'oninput': 'this.setCustomValidity("")'}), max_length=10, min_length=10, required=True)
    dob = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control form-control-sm', 'placeholder':'Date of Birth'}),required=False)
    geo_location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}), required=False)
    state = forms.ChoiceField(choices=[(x.state_id, x.state) for x in StateMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    district = forms.ChoiceField(choices=[(x.district_id, x.district) for x in DistrictMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    city = forms.ChoiceField(choices=[(x.city_id, x.city) for x in CityMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    block_taluka_mandal = forms.ChoiceField(choices=[(x.block_taluka_id, x.block_taluka) for x in BlockTalukaMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Block/Taluka/Mandal'}))
    so_code = NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    asm = NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    rsm = NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    coach = NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    zsm_state_head = NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    cluster_head = NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    is_cement_counter = forms.ChoiceField(choices=CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}))
    counter_type = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    comment = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'placeholder': 'Enter additional information here in <200 chars', 'rows': 5}), max_length=200, required=False)
    unit = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'hidden': 'hidden'}), required=False)
    total_potential = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    wholesale_volume = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth'}))
    retail_volume = forms.CharField(widget=forms.TextInput(attrs={'value':'0','type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'MT/mth', 'readonly': 'readonly'}))
    
    def __init__(self, *args, **kwargs):
        super(CreateNewCounterForm, self).__init__(*args, **kwargs)
        self.fields['total_potential'].label = 'Tot. Pot.(MT/Mth)'
        self.fields['wholesale_volume'].label = 'W/S Vol(MT/Mth)'
        self.fields['retail_volume'].label = 'Calculated Retail Vol(MT/Mth)'
        self.fields['counter_name'].label = 'Counter Name'
        self.fields['email_id'].label = 'Email Id'
        self.fields['phone_number'].label = 'Phone No.'
        self.fields['dob'].label = 'DOB'
        self.fields['geo_location'].label = 'GEO Tag'
        self.fields['block_taluka_mandal'].label = 'Block/Taluka/Mandal'
        self.fields['so_code'].label = 'Sales Officer'
        self.fields['asm'].label = 'ASM'
        self.fields['rsm'].label = 'RSM'
        self.fields['zsm_state_head'].label = 'ZSM/State Head'
        self.fields['cluster_head'].label = 'Cluster Head'
        self.fields['is_cement_counter'].label = 'Is Cement Counter'
        self.fields['counter_type'].label = 'Counter Type'